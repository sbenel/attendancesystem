﻿Imports capaDatos
Public Class clsPermiso
    Public Shared Sub Register(per As Permission)
        Using DB = New BDAsistenciaEntities()
            Try
                DB.Permission.Add(per)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Function ListPermissions() As List(Of Permission)
        Try
            Dim DB = New BDAsistenciaEntities()
            Dim Permissions = From P In DB.Permission Select P
            Return Permissions.ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub Update(id As Integer, presentation As Date, permission As Date, reason As String, state As Boolean, dni As String)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim p = DB.Permission.Find(id)
                p.PresentationDate = presentation
                p.PermissionDate = permission
                p.Reason = reason
                p.State = state
                p.Dni = dni
                DB.SaveChanges()

            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Shared Sub Delete(id As Integer)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim p = DB.Permission.Find(id)
                DB.Permission.Remove(p)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Function Find(id As Integer) As Permission
        Try
            Dim DB = New BDAsistenciaEntities()
            Return DB.Permission.Find(id)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Public Shared Function Buscar(emp As Employee) As List(Of Permission)
    '    Try
    '        Dim DB = New BDAsistenciaEntities()
    '        Dim perms = From p In DB.Permission.Where(Function(per) per.Dni.Equals(emp.Dni)) Select p
    '        Return perms.ToList()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    Public Shared Function FindByDNI(dni As String) As List(Of Permission)
        Try
            Dim DB = New BDAsistenciaEntities()
            Dim perms = DB.Permission.Where(Function(p) p.Dni.Contains(dni)).ToList()
            Return perms
        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class
