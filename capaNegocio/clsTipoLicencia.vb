﻿Imports capaDatos
Public Class clsTipoLicencia
    Public Shared Sub Register(licen As LicenseType)
        Using DB = New BDAsistenciaEntities()
            Try
                DB.LicenseType.Add(licen)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Function ListLicenseType() As List(Of LicenseType)
        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.LicenseType.ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub ModifyLicenseType(new_license As LicenseType)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim old_license = DB.LicenseType.Find(new_license.LicenseTypeId)
                DB.Entry(old_license).CurrentValues.SetValues(new_license)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Sub DeleteLicenseType(id As Integer)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim licen = DB.LicenseType.Find(id)
                DB.LicenseType.Remove(licen)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Function GetMaxTimeSpan(id As Integer) As TimeSpan
        Using DB = New BDAsistenciaEntities()
            Try
                Dim licen = DB.LicenseType.Find(id)
                Dim span As TimeSpan = TimeSpan.FromDays(licen.maxDias)
                Return span
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function GetMaxTimeSpanName(name As String) As TimeSpan
        Using DB = New BDAsistenciaEntities()
            Try
                Dim licen = DB.LicenseType.Find(Function(l) l.Description = name)
                Dim span As TimeSpan = TimeSpan.FromDays(licen.maxDias)
                Return span
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function
    Public Shared Function FindLicencesType(id As Integer) As LicenseType
        Using DB = New BDAsistenciaEntities()
            Try
                Return DB.LicenseType.Find(id)
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function
End Class
