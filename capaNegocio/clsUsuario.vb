﻿Imports capaDatos

Public Class clsUsuario


    ''Iniciar sesión
    Public Shared Function Login(us As String, con As String) As Users

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = From u In DB.users Where u.uSerName Is us And u.userPassword Is con And u.userState = True Select u
            Return user.Single()
        Catch ex As Exception
            Throw New Exception("Error al ingresar: " + ex.Message)
        End Try

    End Function

    ''Registrar Usuario
    Public Shared Sub RegisterUser(UserR As Users)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.Users.Add(UserR)
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al registrar: " + ex.Message)
        End Try
    End Sub


    ''Eliminar Usuario
    Public Shared Sub Delete(id As Integer)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.users.Find(id)
            DB.Users.Remove(user)
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al eliminar: " + ex.Message)
        End Try
    End Sub

    ''Dar de  baja usuario
    Public Shared Sub Down(id As Integer)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.Users.Find(id)
            user.UserState = 0
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al dar de baja: " + ex.Message)
        End Try

    End Sub


    ''Modificar usuario
    Public Shared Sub Update(id As Integer, name As String, password As String, dni As String, state As Boolean)

        Dim DB = New BDAsistenciaEntities()
        Try

            Dim user = DB.Users.Find(id)
            user.USerName = name
            user.UserPassword = password
            user.Dni = dni
            user.UserState = state
            DB.SaveChanges()

        Catch ex As Exception
            Throw New Exception("Error al actualizar: " + ex.Message)
        End Try

    End Sub

    ''Listar Usuario
    Public Shared Function ListUser() As List(Of Users)

        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.Users.ToList()

        Catch ex As Exception
            Throw New Exception("Error al listar: " + ex.Message)
        End Try

    End Function

    ''Buscar Usuarios por id
    Public Shared Function FindUser(id As Integer) As Users

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.Users.Find(id)
            Return user

        Catch ex As Exception
            Throw New Exception("Error al Buscar usuario: " + ex.Message)
        End Try

    End Function


    ''Buscar Usuarios por nombre
    Public Shared Function FindUserByName(nombre As String) As Users

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.Users.First(Function(u) u.USerName = nombre)
            Return user

        Catch ex As Exception
            Throw New Exception("Error al Buscar usuario: " + ex.Message)
        End Try

    End Function


    ''Buscar Usuarios por dni
    Public Shared Function FindUserByDni(dni As String) As Users

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim user = DB.Users.First(Function(u) u.Dni = dni)
            Return user

        Catch ex As Exception
            Throw New Exception("Error al Buscar usuario: " + ex.Message)
        End Try

    End Function


    ''Buscar Usuarios por dni
    Public Shared Function ListUserByDni(dni As String) As List(Of Users)

        Dim DB = New BDAsistenciaEntities()
        Try

            Return DB.Users.Where(Function(u) u.Dni.Contains(dni)).ToList

        Catch ex As Exception
            Throw New Exception("Error al Buscar usuario: " + ex.Message)
        End Try

    End Function

End Class
