﻿Imports capaDatos
Public Class clsLicencia
    Public Shared Sub Register(licen As License)
        Using DB = New BDAsistenciaEntities()
            Try
                DB.License.Add(licen)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Function ListLicenses() As List(Of License)
        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.License.ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub ModifyLicense(new_license As License)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim old_license = DB.License.Find(new_license.LincenseId)
                DB.Entry(old_license).CurrentValues.SetValues(new_license)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
    Public Shared Sub DeleteLicense(id As Integer)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim licen = DB.License.Find(id)
                DB.License.Remove(licen)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub
End Class
