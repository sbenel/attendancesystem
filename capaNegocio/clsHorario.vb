﻿Imports capaDatos
Public Class clsHorario
    Public Shared Function Register(sch As schedule, DB As BDAsistenciaEntities) As Integer
        Try
            DB.schedule.Add(sch)
            DB.SaveChanges()
            Return sch.id
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''Valida si el dia ingresado esta dentro del detalle de horario
    Public Shared Function ValidateDays(day As Integer) As Integer

        Dim DB = New BDAsistenciaEntities()

        Try
            Dim vd = DB.ScheduleDetail.Where(Function(c) c.Day = day And c.Schedule.State = True).Count()
            Return vd
        Catch ex As Exception
            Throw ex
        End Try

    End Function


    ''Lista los horarios
    Public Shared Function ListSchedule() As List(Of Schedule)
        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.Schedule.ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''Modifica el horario
    Public Shared Sub ModifySchedule(new_sched As Schedule)

        Using DB = New BDAsistenciaEntities()

            Try

                Dim old_sche = DB.Schedule.Find(new_sched.Id)
                DB.Entry(old_sche).CurrentValues.SetValues(new_sched)
                DB.SaveChanges()

            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    ''Guardar
    Public Shared Function LastSaved() As Integer

        Using DB = New BDAsistenciaEntities()

            Try

                Return DB.Schedule.Last().Id

            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    ''Buscar el horario por el dni 

    Public Shared Function SearchScheduleForDNI(dni As String) As List(Of Schedule)

        Dim DB = New BDAsistenciaEntities()

        Try

            Return DB.Schedule.Where(Function(s) s.Dni = dni).ToList()

        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class