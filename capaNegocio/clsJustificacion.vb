﻿Imports capaDatos
Public Class clsJustificacion
    Public Shared Function List() As List(Of Justification)
        Try
            Dim DB = New BDAsistenciaEntities()
            Dim Justifications = From J In DB.Justification Select J
            Return Justifications.ToList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function List(dni As String) As List(Of Justification)
        Try
            Dim DB = New BDAsistenciaEntities()
            Dim justify = From j In DB.Justification Where j.Assistance.Dni = dni Select j
            Return justify.ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub Register(Justification As justification)
        Try
            Dim DB = New BDAsistenciaEntities()
            DB.justification.Add(Justification)
            DB.SaveChanges()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
End Class
