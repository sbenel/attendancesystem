﻿Imports capaDatos
Public Class clsDetalleHorario

    Public Shared Sub Register(sch As scheduleDetail, DB As BDAsistenciaEntities)
        Try
            DB.scheduleDetail.Add(sch)
            DB.SaveChanges()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Function ListSchedule() As List(Of ScheduleDetail)
        Using DB = New BDAsistenciaEntities()
            Try
                Return DB.ScheduleDetail.ToList()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Sub ModifyScheduleDetail(new_sched As ScheduleDetail)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim old_sche = DB.ScheduleDetail.Find(new_sched.Id)
                DB.Entry(old_sche).CurrentValues.SetValues(new_sched)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Shared Function SearchDetailForScheduleId(id As Integer) As List(Of ScheduleDetail)
        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.ScheduleDetail.Where(Function(s) s.ScheduleId = id).ToList()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
