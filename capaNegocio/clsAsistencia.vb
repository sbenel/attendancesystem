﻿Imports capaDatos
Public Class clsAsistencia

    ''Buscar los datos del horario del empleado

    Public Shared Function Info(dni As String) As scheduleDetail
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim sd = DB.scheduleDetail.First(Function(s) s.schedule.employee.dni = dni)
            Return sd
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try
    End Function

    ''Buscar la asistencia de una fecha 
    Public Shared Function CountAsis(Dni As String) As Integer
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim count As Integer
            'count = DB.Assistance.Count(Function(a) a.fecha = fecha And a.Dni = Dni)
            count = (From a In DB.assistance Where a.fecha = Today And a.dni = Dni Select a).Count()
            Console.Write(count)
            Return count
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try

    End Function

    ''Buscar la asistencia de una fecha y hora de salidad nula 
    Public Shared Function CountOutNull(Dni As String) As Integer
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim count As Integer
            count = (From a In DB.assistance Where a.fecha = Today And a.dni = Dni And a.outHour Is Nothing Select a).Count()
            Console.Write(count)
            Return count
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try

    End Function


    Public Shared Function CountInNull(Dni As String) As Integer
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim E = DB.employee.Find(Dni).assistance.Where(Function(A) A.fecha = Today And A.inHour Is Nothing).Count()

            Return E
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try
    End Function
    ''Registrar Asistencia
    Public Shared Sub RegisterAssistance(Asist As assistance)
        Dim DB = New BDAsistenciaEntities()
        Try
            DB.assistance.Add(Asist)
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al registrar: " + ex.Message)
        End Try
    End Sub
    Public Shared Sub RegisterAssistanceNulls(Asist As assistance, DB As BDAsistenciaEntities)
        Try
            Dim assistance = DB.assistance.Add(Asist)
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al registrar: " + ex.Message)
        End Try
    End Sub

    ''Eliminar Asistencia
    'Public Shared Sub Delete(id As Integer)

    '    Dim DB = New BDAsistenciaEntities()
    '    Try
    '        Dim assistance = From c In DB.Assistance Where c.AssistanceId.Equals(id) Select c
    '        DB.Assistance.Remove(assistance)
    '        DB.SaveChanges()
    '    Catch ex As Exception
    '        Throw New Exception("Error al eliminar: " + ex.Message)
    '    End Try
    'End Sub
    ''Modificar Asistencia
    Public Shared Sub UpdateOut(Dni As String, fin As TimeSpan, fecha As Date)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assistance = DB.assistance.First(Function(ax) ax.dni = Dni And ax.fecha = fecha)
            assistance.outHour = fin
            DB.SaveChanges()

        Catch ex As Exception
            Throw New Exception("Error al registrar la hora de salida: " + ex.Message)
        End Try

    End Sub

    Public Shared Sub UpdateIn(Dni As String, inicio As TimeSpan, fecha As Date)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assistance = DB.assistance.First(Function(ax) ax.dni = Dni And ax.fecha = fecha)
            assistance.inHour = inicio
            DB.SaveChanges()

        Catch ex As Exception
            Throw New Exception("Error al registrar la hora de entrada: " + ex.Message)
        End Try

    End Sub
    'Listar Asistencias
    Public Shared Function List(dni As String) As List(Of assistance)
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assisa = (From a In DB.assistance Where a.dni = dni And a.inHour <> Nothing Select a).ToList()
            Return assisa
        Catch ex As Exception
            Throw New Exception("Error al listar: " + ex.Message)
        End Try
    End Function
    'Buscar Asistencias por una fecha
    Public Shared Function ListDate(Fecha As Date?) As List(Of assistance)
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assisa = (From a In DB.assistance Where a.fecha = Fecha And a.inHour <> Nothing Select a).ToList()
            Return assisa
        Catch ex As Exception
            Throw New Exception("Error al listar: " + ex.Message)
        End Try
    End Function
    ''Buscar Asistencia por id
    Public Shared Function FindById(id As Integer) As assistance
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assistance = DB.assistance.Find(id)
            Return assistance
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try
    End Function
    ''Buscar Asistencia por dni
    Public Shared Function FindByDni(Dni As String) As assistance
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim assistance = DB.assistance.First(Function(c) c.dni = Dni)
            Return assistance
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try

    End Function
    ''Valida si tiene horario 
    Public Shared Function ValidationShedule(dni As String) As Boolean
        Dim DB = New BDAsistenciaEntities()
        Try
            Dim ex = DB.employee.Find(dni)
            If ex.state = True And ex.contract.Count > 0 Then
                If (ex.contract.Where(Function(e) e.state = True).Count() > 0) Then
                    If ex.schedule.Count > 0 Then
                        If (ex.schedule.Where(Function(e) e.state = True).Count() > 0) Then
                            Return True
                        Else
                            Return False
                        End If
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
