﻿Imports capaDatos

Public Class clsContrato

    ''Registrar contrato
    Public Shared Sub RegisterContrat(ContratR As Contract)

        Dim DB = New BDAsistenciaEntities()
        Try
            DB.contract.Add(ContratR)
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al registrar: " + ex.Message)
        End Try
    End Sub


    ''Eliminar contrato
    Public Shared Sub Delete(id As Integer)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim contrat = DB.Contract.Find(id)
            DB.Contract.Remove(contrat)
            DB.SaveChanges()

        Catch ex As Exception
            Throw New Exception("Error al eliminar: " + ex.Message)
        End Try
    End Sub

    ''Dar de  baja contrato
    Public Shared Sub Down(id As Integer)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim contrat = DB.Contract.Find(id)
            contrat.State = 0
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al dar de baja: " + ex.Message)
        End Try

    End Sub


    ''Modificar contrato
    Public Shared Sub Update(id As Integer, inicio As Date, fin As Date, salario As Double, extra As Boolean, estado As Boolean)

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim contrat = DB.Contract.Find(id)
            contrat.StartDate = inicio
            contrat.FinishDate = fin
            contrat.Mount = salario
            contrat.ExtraHours = extra
            contrat.State = estado
            DB.SaveChanges()
        Catch ex As Exception
            Throw New Exception("Error al actualizar: " + ex.Message)
        End Try

    End Sub


    ''Validar si tiene horas extra 

    Public Shared Function ExtraTime(dni As String) As Boolean

        Dim DB = New BDAsistenciaEntities()

        Try

            Dim c = (From cx In DB.Contract Where cx.Dni = dni And cx.State = True And cx.ExtraHours = True Select cx).Count()
            If c > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception("Error al buscar: " + ex.Message)
        End Try


    End Function


    ''Listar contratos
    Public Shared Function ListContrat() As List(Of Contract)

        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.Contract.ToList()
        Catch ex As Exception
            Throw New Exception("Error al listar: " + ex.Message)
        End Try

    End Function

    ''Buscar contrato por id
    Public Shared Function FindContrat(id As Integer) As Contract

        Dim DB = New BDAsistenciaEntities()
        Try
            Dim contract = DB.Contract.Find(id)
            Return contract
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try

    End Function

    ''Buscar contrato por dni
    Public Shared Function FindContratByDni(Dni As String) As List(Of Contract)

        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.Contract.Where(Function(co) co.Dni.Contains(Dni)).ToList()
        Catch ex As Exception
            Throw New Exception("Error al Buscar: " + ex.Message)
        End Try

    End Function


End Class
