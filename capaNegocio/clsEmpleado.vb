﻿Imports capaDatos
Public Class clsEmpleado
    Public Shared Sub Register(emp As Employee)
        Using DB = New BDAsistenciaEntities()
            Try
                DB.Employee.Add(emp)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Shared Function Validation(dni As String) As Boolean
        Using DB = New BDAsistenciaEntities()
            Try
                Dim value As Boolean
                Dim sx = DB.Schedule.Count(Function(e) e.Dni = dni)
                Dim cx = DB.Contract.Count(Function(e) e.Dni = dni)
                Dim lx = DB.License.Count(Function(e) e.Dni = dni)
                Dim px = DB.Permission.Count(Function(e) e.Dni = dni)
                Dim us = DB.Users.Count(Function(e) e.Dni = dni)

                If sx > 0 Or cx > 0 Or lx > 0 Or px > 0 Or us > 0 Then
                    Return value = False
                Else
                    Return value = True
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Sub DownEmployee(dni As String)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim ex = DB.Employee.First(Function(e) e.Dni = dni)
                ex.State = 0

                If ex.Schedule.Count > 0 Then
                    Dim sx = ex.Schedule.First(Function(s) s.State = True)
                    sx.State = 0
                End If

                If ex.Contract.Count > 0 Then
                    Dim cx = ex.Contract.First(Function(c) c.State = True)
                    cx.State = 0
                End If

                If ex.License.Count > 0 Then
                    Dim lx = ex.License.First(Function(l) l.State = True)
                    lx.State = 0
                End If

                If ex.Permission.Count > 0 Then
                    Dim px = ex.Permission.First(Function(p) p.State = True)
                    px.State = 0
                End If

                If ex.Users.Count > 0 Then
                    Dim us = ex.Users.First(Function(u) u.UserState = True)
                    us.UserState = 0
                End If

                DB.SaveChanges()

            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub


    Public Shared Function ListEmployee() As List(Of Employee)
        Using DB = New BDAsistenciaEntities()
            Try
                Return DB.Employee.ToList()
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Function SearchEmployee(dni As String) As List(Of Employee)
        Using DB = New BDAsistenciaEntities()
            Try

                Dim list = DB.employee.Where(Function(e) e.dni.Contains(dni)).ToList()
                Return list
            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Function

    Public Shared Sub DeleteEmployee(dni As String)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim emp = DB.Employee.Find(dni)
                DB.Employee.Remove(emp)
                DB.SaveChanges()

            Catch ex As Exception
                Throw ex
            End Try
        End Using
    End Sub

    Public Shared Sub ModifyEmployee(emp As Employee)
        Using DB = New BDAsistenciaEntities()
            Try
                Dim old_emp = DB.Employee.Find(emp.Dni)
                DB.Entry(old_emp).CurrentValues.SetValues(emp)
                DB.SaveChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Using

    End Sub

    ''Buscar DNI/ esto lo uso para ver si el dni ingresado en el contrato existe
    Public Shared Function FindDni(DNI As String) As Integer

        Dim DB = New BDAsistenciaEntities()
        Try
            'Dim employee = (From e In DB.Employee Where e.Dni.Equals(DNI) Select e).Count()
            Dim count As Integer
            count = DB.employee.Where(Function(e) e.dni = DNI).Count()
            Return count
        Catch ex As Exception
            Throw New Exception("Error en el DNI: " + ex.Message)
        End Try
    End Function
    ''Buscar por DNI
    Public Shared Function FindByDni(DNI As String) As employee
        Dim DB = New BDAsistenciaEntities()
        Try
            Return DB.employee.Find(DNI)
        Catch ex As Exception
            Throw New Exception("Error en el DNI: " + ex.Message)
        End Try
    End Function

End Class
