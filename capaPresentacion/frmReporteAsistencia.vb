﻿Imports capaNegocio

Public Class frmReporteAsistencia
    Private Sub frmReporteAsistencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click

    End Sub
    Private Sub ListAllAsistencia(list)
        Try
            DgvAsistencia.Columns.Clear()
            DgvAsistencia.DataSource = list
            DgvAsistencia.Columns(0).HeaderText = "Id"
            DgvAsistencia.Columns(1).HeaderText = "Fecha"
            DgvAsistencia.Columns(2).HeaderText = "Hora Entrada"
            DgvAsistencia.Columns(3).HeaderText = "Hora Salida"
            DgvAsistencia.Columns(4).HeaderText = "DNI"
            DgvAsistencia.Columns.Remove("Employee")
            DgvAsistencia.Columns.Remove("Justification")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub BtnBuscarDNI_Click(sender As Object, e As EventArgs) Handles BtnBuscarDNI.Click
        Try
            Dim dni = TxtDni.Text
            ListAllAsistencia(clsAsistencia.List(dni))
            TxtDni.Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnBuscarFecha_Click(sender As Object, e As EventArgs) Handles btnBuscarFecha.Click
        Try
            TxtDni.Clear()
            Dim fecha = dtpFecha.Text
            ListAllAsistencia(clsAsistencia.ListDate(fecha))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class