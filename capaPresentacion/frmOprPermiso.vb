﻿Imports capaDatos
Imports capaNegocio
Public Class frmOprPermiso

    Private permission As capaDatos.Permission
    Dim id As String
    Private Sub setNewPermission()
        permission = New capaDatos.Permission
        permission.PresentationDate = dtPresentation.Value
        permission.PermissionDate = dtPermission.Value
        permission.Reason = rtReason.Text
        permission.State = rbEstado.Checked
        permission.Dni = txtDni.Text

    End Sub
    Private Sub frmOprPermiso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        listPermission()
    End Sub

    Private Sub AddButtonColumn(text As String)
        Dim bt As New DataGridViewButtonColumn With {
            .Text = text,
            .Name = "Btn" & text,
            .UseColumnTextForButtonValue = True,
            .FlatStyle = FlatStyle.Flat
        }
        bt.CellTemplate.Style.ForeColor = Color.Black
        dgwPermiso.Columns.Add(bt)
    End Sub

    Private Sub UpdatePermission(id As Integer)
        Try

            Dim p = clsPermiso.Find(id)
            dtPresentation.Value = p.PresentationDate
            dtPermission.Value = p.PermissionDate
            rtReason.Text = p.Reason
            txtDni.Text = p.Dni
            rbEstado.Text = p.State
            BtnRegister.Text = "Modificar"

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub listPermission()
        Try
            dgwPermiso.Columns.Clear()
            dgwPermiso.DataSource = clsPermiso.ListPermissions()
            dgwPermiso.Columns.Remove("Employee")
            AddButtonColumn("Eliminar")
            AddButtonColumn("Modificar")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub DeletePermision(id As Integer)
        Try
            clsPermiso.Delete(id)
            listPermission()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub listPermissionByEmpleado()
        Try
            dgwPermiso.Columns.Clear()
            dgwPermiso.DataSource = clsPermiso.FindByDNI(txtDNIBuscar.Text)
            dgwPermiso.Columns(0).HeaderText = "Id"
            dgwPermiso.Columns(1).HeaderText = "Inicio de Permiso"
            dgwPermiso.Columns(2).HeaderText = "Fin de Permiso"
            dgwPermiso.Columns(3).HeaderText = "Razón"
            dgwPermiso.Columns(4).HeaderText = "Estado"
            dgwPermiso.Columns.Remove("Employee")
            AddButtonColumn("Eliminar")
            AddButtonColumn("Modificar")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub ClearControls()
        txtDni.Clear()
        rtReason.Clear()
        rbEstado.Checked = False
        dtPermission.Value = DateTime.Now()
        dtPresentation.Value = DateTime.Now()
    End Sub

    Private Sub dgwPermiso_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgwPermiso.CellContentClick

        id = dgwPermiso.CurrentRow.Cells(0).Value.ToString

        Dim senderGrid = DirectCast(sender, DataGridView)
        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            If e.ColumnIndex = 6 Then
                DeletePermision(Integer.Parse(id))
            ElseIf e.ColumnIndex = 7 Then
                UpdatePermission(Integer.Parse(id))
            End If
        End If
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Try
            listPermissionByEmpleado()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        If BtnRegister.Text = "Modificar" Then
            Try
                clsPermiso.Update(id, dtPresentation.Value, dtPermission.Value, rtReason.Text, rbEstado.Checked, txtDni.Text)
                BtnRegister.Text = "Registrar"
                ClearControls()
                listPermission()
                MessageBox.Show("Modificación exitosa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show("Error en la modificacion" + ex.Message.ToString, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        Else
            Try
                Dim countE As Integer = clsEmpleado.FindDni(txtDni.Text)
                If (txtDni.TextLength = 0) Then
                    MessageBox.Show("Es necesario completar todos los campos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ClearControls()
                ElseIf txtDni.TextLength = 8 And countE = 1 Then
                    setNewPermission()
                    clsPermiso.Register(permission)
                    ClearControls()
                    listPermission()
                    MessageBox.Show("Registro exitoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("El DNI no ha sido ingresado correctamente")
                End If
            Catch ex As Exception
                MessageBox.Show("Error al registrar" + ex.Message.ToString, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If
    End Sub


End Class