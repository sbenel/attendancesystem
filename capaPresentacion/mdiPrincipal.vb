﻿Imports System.Windows.Forms
Imports Stimulsoft
Imports Stimulsoft.Report

Public Class mdiPrincipal
    Dim user As capaDatos.Users
    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
        ' Cree una nueva instancia del formulario secundario.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Ventana " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: agregue código aquí para abrir el archivo.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: agregue código aquí para guardar el contenido actual del formulario en un archivo.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Utilice My.Computer.Clipboard.GetText() o My.Computer.Clipboard.GetData para recuperar la información del Portapapeles.
    End Sub

    Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ToolBarToolStripMenuItem.Click
        Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Cierre todos los formularios secundarios del principal.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub mdiPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LogIn()
    End Sub

    Private Sub TmrPrincipal_Tick(sender As Object, e As EventArgs) Handles TmrPrincipal.Tick
        SetTime()
    End Sub
    Private Sub SetTime()
        Try
            TxtClock.Text = DateTime.Now.ToString("hh:mm")
            LblDate.Text = DateTime.Now.ToString("dddd, dd/MM/yyyy")
        Catch ex As Exception
            MessageBox.Show("Algo está fallando")
        End Try
    End Sub
    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles BtnLogout.Click
        LogOut()
    End Sub

    Private Sub ClearData()
        user = Nothing
        LblUsername.Text = ""
    End Sub
    Private Sub LogOut()
        ClearData()
        LogIn()
    End Sub

    Private Sub LogIn()
        Dim objLogin As New frmLogin
        objLogin.ShowDialog()
        If objLogin.User IsNot Nothing Then
            user = objLogin.User
            LblUsername.Text = user.employee.name + " " + user.employee.lastname
        Else
            Application.Exit()
        End If
    End Sub

    Private Sub CerrarSesiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarSesiónToolStripMenuItem.Click
        LogOut()
    End Sub

    Private Sub AsistenciasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AsistenciasToolStripMenuItem.Click
        Dim objAsistencia As New frmOprAsistencia
        objAsistencia.ShowDialog()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim objUsuario As New frmManUsers
        objUsuario.ShowDialog()
    End Sub

    Private Sub ContratosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ContratosToolStripMenuItem.Click
        Dim objContrato As New frmManContratos
        objContrato.ShowDialog()
    End Sub

    Private Sub GestionarEmpleadoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionarEmpleadoToolStripMenuItem.Click
        Dim objMantenimientoEmpleado As New frmManEmployee
        objMantenimientoEmpleado.ShowDialog()
    End Sub

    Private Sub ConsultasYReportesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem.Click

    End Sub

    Private Sub TipoLicenciaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TipoLicenciaToolStripMenuItem.Click
        Dim objManTipoLicencia As New frmManTipoLicencia
        objManTipoLicencia.ShowDialog()
    End Sub

    Private Sub HorarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HorarioToolStripMenuItem.Click
        Dim objOprHorario As New frmOprHorario
        objOprHorario.ShowDialog()
    End Sub

    Private Sub JustificacionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JustificacionesToolStripMenuItem.Click
        Dim objOprJustificaciones As New frmOprJustifacion
        objOprJustificaciones.ShowDialog()
    End Sub

    Private Sub PermisosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PermisosToolStripMenuItem.Click
        Dim objOprPermisos As New frmOprPermiso
        objOprPermisos.ShowDialog()
    End Sub

    Private Sub LicenciasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LicenciasToolStripMenuItem.Click
        Dim objOprLicencias As New frmOprLicencia
        objOprLicencias.ShowDialog()
    End Sub

    Private Sub ConsultarAsistenciaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarAsistenciaToolStripMenuItem.Click
        Dim objConAsistencia As New frmReporteAsistencia
        objConAsistencia.ShowDialog()
    End Sub

    Private Sub ConsolidadoAsistenciaMensualToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsolidadoAsistenciaMensualToolStripMenuItem.Click
        Dim ruta() = Split(My.Computer.FileSystem.CurrentDirectory, "bin\", 2)
        Dim Report = New StiReport()
        Report.Load(ruta(0) & "Reports\Reporte_Asistencias.mrt")
        Report.Show()
    End Sub

    Private Sub ReporteFaltasYTardanzasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteFaltasYTardanzasToolStripMenuItem.Click
        Dim ruta() = Split(My.Computer.FileSystem.CurrentDirectory, "bin\", 2)
        Dim Report = New StiReport()
        Report.Load(ruta(0) & "Reports\Reporte_Faltas.mrt")
        Report.Show()
    End Sub

    Private Sub ReporteLicenciasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteLicenciasToolStripMenuItem.Click
        Dim ruta() = Split(My.Computer.FileSystem.CurrentDirectory, "bin\", 2)
        Dim Report = New StiReport()
        Report.Load(ruta(0) & "Reports\Reporte_Licencias.mrt")
        Report.Show()
    End Sub

    Private Sub ReporteTardanzasToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs)
        Dim objMantenimientoEmpleado As New frmManEmployee
        objMantenimientoEmpleado.ShowDialog()
    End Sub

    Private Sub ReporteTardanzasToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles ReporteTardanzasToolStripMenuItem.Click
        Dim ruta() = Split(My.Computer.FileSystem.CurrentDirectory, "bin\", 2)
        Dim Report = New StiReport()
        Report.Load(ruta(0) & "Reports\Reporte_Tardanzas.mrt")
        Report.Show()
    End Sub

    Private Sub ToolStripButton4_Click(sender As Object, e As EventArgs) Handles ToolStripButton4.Click
        Dim objOprHorario As New frmOprHorario
        objOprHorario.ShowDialog()
    End Sub

    Private Sub ToolStripButton5_Click(sender As Object, e As EventArgs) Handles ToolStripButton5.Click
        Dim objAsistencia As New frmOprAsistencia
        objAsistencia.ShowDialog()
    End Sub

    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        Dim objContrato As New frmManContratos
        objContrato.ShowDialog()
    End Sub

    Private Sub ToolStripButton2_Click_1(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Dim objMantenimientoEmpleado As New frmManEmployee
        objMantenimientoEmpleado.ShowDialog()
    End Sub
End Class
