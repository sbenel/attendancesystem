﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmManEmployee
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmManEmployee))
        Me.PnlEmployees = New System.Windows.Forms.Panel()
        Me.SpcEmployee = New System.Windows.Forms.SplitContainer()
        Me.RbtnState = New System.Windows.Forms.RadioButton()
        Me.CbxSex = New System.Windows.Forms.ComboBox()
        Me.BtnRegister = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TxtDni = New System.Windows.Forms.TextBox()
        Me.TxtLastname = New System.Windows.Forms.TextBox()
        Me.TxtEmail = New System.Windows.Forms.TextBox()
        Me.TxtAdress = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtNombre = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtPhone = New System.Windows.Forms.TextBox()
        Me.DgvEmpl = New System.Windows.Forms.DataGridView()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TxtBuscar = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PnlEmployees.SuspendLayout()
        CType(Me.SpcEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcEmployee.Panel1.SuspendLayout()
        Me.SpcEmployee.Panel2.SuspendLayout()
        Me.SpcEmployee.SuspendLayout()
        CType(Me.DgvEmpl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PnlEmployees
        '
        Me.PnlEmployees.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.PnlEmployees.Controls.Add(Me.SpcEmployee)
        Me.PnlEmployees.Controls.Add(Me.Label7)
        Me.PnlEmployees.Location = New System.Drawing.Point(1, 0)
        Me.PnlEmployees.Name = "PnlEmployees"
        Me.PnlEmployees.Size = New System.Drawing.Size(1254, 639)
        Me.PnlEmployees.TabIndex = 0
        '
        'SpcEmployee
        '
        Me.SpcEmployee.Location = New System.Drawing.Point(11, 81)
        Me.SpcEmployee.Name = "SpcEmployee"
        '
        'SpcEmployee.Panel1
        '
        Me.SpcEmployee.Panel1.Controls.Add(Me.RbtnState)
        Me.SpcEmployee.Panel1.Controls.Add(Me.CbxSex)
        Me.SpcEmployee.Panel1.Controls.Add(Me.BtnRegister)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label5)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label1)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label3)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label10)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtDni)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtLastname)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtEmail)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtAdress)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label6)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label2)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label4)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label9)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtNombre)
        Me.SpcEmployee.Panel1.Controls.Add(Me.Label8)
        Me.SpcEmployee.Panel1.Controls.Add(Me.TxtPhone)
        '
        'SpcEmployee.Panel2
        '
        Me.SpcEmployee.Panel2.Controls.Add(Me.DgvEmpl)
        Me.SpcEmployee.Panel2.Controls.Add(Me.BtnSearch)
        Me.SpcEmployee.Panel2.Controls.Add(Me.Label11)
        Me.SpcEmployee.Panel2.Controls.Add(Me.TxtBuscar)
        Me.SpcEmployee.Size = New System.Drawing.Size(1230, 543)
        Me.SpcEmployee.SplitterDistance = 409
        Me.SpcEmployee.TabIndex = 26
        '
        'RbtnState
        '
        Me.RbtnState.AutoSize = True
        Me.RbtnState.Checked = True
        Me.RbtnState.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RbtnState.ForeColor = System.Drawing.Color.White
        Me.RbtnState.Location = New System.Drawing.Point(106, 400)
        Me.RbtnState.Name = "RbtnState"
        Me.RbtnState.Size = New System.Drawing.Size(78, 27)
        Me.RbtnState.TabIndex = 8
        Me.RbtnState.TabStop = True
        Me.RbtnState.Text = "Activo"
        Me.RbtnState.UseVisualStyleBackColor = True
        '
        'CbxSex
        '
        Me.CbxSex.FormattingEnabled = True
        Me.CbxSex.Items.AddRange(New Object() {"Femenino", "Masculino"})
        Me.CbxSex.Location = New System.Drawing.Point(106, 228)
        Me.CbxSex.Name = "CbxSex"
        Me.CbxSex.Size = New System.Drawing.Size(251, 28)
        Me.CbxSex.TabIndex = 4
        '
        'BtnRegister
        '
        Me.BtnRegister.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnRegister.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRegister.FlatAppearance.BorderSize = 0
        Me.BtnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRegister.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRegister.ForeColor = System.Drawing.Color.White
        Me.BtnRegister.Location = New System.Drawing.Point(38, 470)
        Me.BtnRegister.Name = "BtnRegister"
        Me.BtnRegister.Size = New System.Drawing.Size(319, 39)
        Me.BtnRegister.TabIndex = 9
        Me.BtnRegister.Text = "Registrar"
        Me.BtnRegister.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(65, 99)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 23)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Dni"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(56, 228)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 23)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Sexo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(44, 400)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 23)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Estado"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(30, 271)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(81, 23)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Dirección"
        '
        'TxtDni
        '
        Me.TxtDni.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDni.Location = New System.Drawing.Point(106, 96)
        Me.TxtDni.Name = "TxtDni"
        Me.TxtDni.Size = New System.Drawing.Size(251, 29)
        Me.TxtDni.TabIndex = 1
        '
        'TxtLastname
        '
        Me.TxtLastname.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtLastname.Location = New System.Drawing.Point(106, 182)
        Me.TxtLastname.Name = "TxtLastname"
        Me.TxtLastname.Size = New System.Drawing.Size(251, 29)
        Me.TxtLastname.TabIndex = 3
        '
        'TxtEmail
        '
        Me.TxtEmail.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtEmail.Location = New System.Drawing.Point(106, 354)
        Me.TxtEmail.Name = "TxtEmail"
        Me.TxtEmail.Size = New System.Drawing.Size(251, 29)
        Me.TxtEmail.TabIndex = 7
        '
        'TxtAdress
        '
        Me.TxtAdress.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtAdress.Location = New System.Drawing.Point(106, 268)
        Me.TxtAdress.Name = "TxtAdress"
        Me.TxtAdress.Size = New System.Drawing.Size(251, 29)
        Me.TxtAdress.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(35, 142)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 23)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Nombre"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(36, 185)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 23)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Apellido"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(43, 357)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 23)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Correo"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(35, 314)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 23)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Teléfono"
        '
        'TxtNombre
        '
        Me.TxtNombre.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNombre.Location = New System.Drawing.Point(106, 139)
        Me.TxtNombre.Name = "TxtNombre"
        Me.TxtNombre.Size = New System.Drawing.Size(251, 29)
        Me.TxtNombre.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(19, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(372, 32)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Ingresa los datos del empleado"
        '
        'TxtPhone
        '
        Me.TxtPhone.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtPhone.Location = New System.Drawing.Point(106, 311)
        Me.TxtPhone.Name = "TxtPhone"
        Me.TxtPhone.Size = New System.Drawing.Size(251, 29)
        Me.TxtPhone.TabIndex = 6
        '
        'DgvEmpl
        '
        Me.DgvEmpl.AllowUserToAddRows = False
        Me.DgvEmpl.AllowUserToDeleteRows = False
        Me.DgvEmpl.AllowUserToOrderColumns = True
        Me.DgvEmpl.AllowUserToResizeColumns = False
        Me.DgvEmpl.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvEmpl.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvEmpl.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvEmpl.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvEmpl.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DgvEmpl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvEmpl.DefaultCellStyle = DataGridViewCellStyle2
        Me.DgvEmpl.EnableHeadersVisualStyles = False
        Me.DgvEmpl.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvEmpl.Location = New System.Drawing.Point(28, 74)
        Me.DgvEmpl.Name = "DgvEmpl"
        Me.DgvEmpl.ReadOnly = True
        Me.DgvEmpl.RowHeadersVisible = False
        Me.DgvEmpl.RowHeadersWidth = 51
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvEmpl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvEmpl.Size = New System.Drawing.Size(773, 450)
        Me.DgvEmpl.TabIndex = 28
        '
        'BtnSearch
        '
        Me.BtnSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnSearch.FlatAppearance.BorderSize = 0
        Me.BtnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSearch.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSearch.ForeColor = System.Drawing.Color.White
        Me.BtnSearch.Location = New System.Drawing.Point(715, 30)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(86, 26)
        Me.BtnSearch.TabIndex = 11
        Me.BtnSearch.Text = "Buscar"
        Me.BtnSearch.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(25, 35)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(64, 23)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Buscar:"
        '
        'TxtBuscar
        '
        Me.TxtBuscar.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBuscar.Location = New System.Drawing.Point(77, 32)
        Me.TxtBuscar.Name = "TxtBuscar"
        Me.TxtBuscar.Size = New System.Drawing.Size(632, 29)
        Me.TxtBuscar.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(28, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(495, 46)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Administración de empleados"
        '
        'frmManEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1252, 630)
        Me.Controls.Add(Me.PnlEmployees)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(1270, 677)
        Me.MinimizeBox = False
        Me.Name = "frmManEmployee"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Empleado"
        Me.PnlEmployees.ResumeLayout(False)
        Me.PnlEmployees.PerformLayout()
        Me.SpcEmployee.Panel1.ResumeLayout(False)
        Me.SpcEmployee.Panel1.PerformLayout()
        Me.SpcEmployee.Panel2.ResumeLayout(False)
        Me.SpcEmployee.Panel2.PerformLayout()
        CType(Me.SpcEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcEmployee.ResumeLayout(False)
        CType(Me.DgvEmpl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PnlEmployees As Panel
    Friend WithEvents TxtNombre As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TxtDni As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents BtnRegister As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents SpcEmployee As SplitContainer
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents TxtLastname As TextBox
    Friend WithEvents TxtEmail As TextBox
    Friend WithEvents TxtAdress As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TxtPhone As TextBox
    Friend WithEvents BtnSearch As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents TxtBuscar As TextBox
    Friend WithEvents CbxSex As ComboBox
    Friend WithEvents RbtnState As RadioButton
    Friend WithEvents DgvEmpl As DataGridView
End Class
