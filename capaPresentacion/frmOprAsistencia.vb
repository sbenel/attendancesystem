﻿Imports capaNegocio

Public Class frmOprAsistencia

    Private assistance As capaDatos.assistance

    Private Sub frmOprAsistencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub BtnAsis_Click(sender As Object, e As EventArgs) Handles BtnAsis.Click
        Try

            Dim sh As Boolean = clsAsistencia.ValidationShedule(txtDniBuscar.Text)
            If sh Then
                Dim c = clsContrato.ExtraTime(txtDniBuscar.Text)

                If c Then
                    If clsAsistencia.CountInNull(txtDniBuscar.Text) = 1 Then
                        clsAsistencia.UpdateIn(txtDniBuscar.Text, DateTime.Now.TimeOfDay, DateTime.Now.Date)
                        'ListTable()
                        MessageBox.Show("Bienvenido ")
                        BtnAsis.Text = "Salida"

                    ElseIf (clsAsistencia.CountOutNull(txtDniBuscar.Text) = 1) Then

                        clsAsistencia.UpdateOut(txtDniBuscar.Text, DateTime.Now.TimeOfDay, DateTime.Now.Date)
                        'ListTable()
                        MessageBox.Show("Hasta Luego ")
                        BtnAsis.Text = "Entrada"
                        Dispose()

                    ElseIf (clsAsistencia.CountAsis(txtDniBuscar.Text) = 1) Then
                        MessageBox.Show("Solo puede registrar una entrada y una salida por dia")
                    Else
                        Dim new_asistance As New capaDatos.assistance()
                        new_asistance.dni = txtDniBuscar.Text
                        new_asistance.fecha = Today
                        new_asistance.inHour = DateTime.Now.TimeOfDay
                        new_asistance.outHour = Nothing
                        clsAsistencia.RegisterAssistance(new_asistance)
                        MessageBox.Show("Bienvenido")
                    End If
                Else
                    Dim time As Integer = DateTime.Today.DayOfWeek()
                    Dim contador As Integer = clsHorario.ValidateDays(time)
                    If (contador = 1) Then
                        If (clsAsistencia.CountInNull(txtDniBuscar.Text) = 1) Then
                            clsAsistencia.UpdateIn(txtDniBuscar.Text, DateTime.Now.TimeOfDay, DateTime.Now.Date)
                            'ListTable()
                            MessageBox.Show("Bienvenido ")
                            BtnAsis.Text = "Salida"

                        ElseIf (clsAsistencia.CountOutNull(txtDniBuscar.Text) = 1) Then
                            clsAsistencia.UpdateOut(txtDniBuscar.Text, DateTime.Now.TimeOfDay, DateTime.Now.Date)
                            'ListTable()
                            MessageBox.Show("Hasta Luego ")
                            BtnAsis.Text = "Entrada"

                        ElseIf (clsAsistencia.CountAsis(txtDniBuscar.Text) = 1) Then
                            MessageBox.Show("Solo puede registrar una entrada y una salida por dia ")
                        End If
                    Else
                        MessageBox.Show("El día de hoy no tiene que asistir")
                    End If
                End If
            Else
                MessageBox.Show("Usted no tiene horarios de trabajo vigentes, contrato o el dni ingresado es incorrecto")
            End If
        Catch ex As Exception
            MessageBox.Show("Error al registrar: " + ex.Message)
        End Try


    End Sub



    'Private Sub ListTable()
    '    Try
    '        dgvAsistencia.DataSource = clsAsistencia.List(txtDniBuscar.Text)
    '        dgvAsistencia.Columns.Remove("Employee")
    '        dgvAsistencia.Columns.Remove("Justification")
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try


    'End Sub

End Class