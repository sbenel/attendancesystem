﻿Imports capaNegocio
Public Class frmOprJustifacion
    Private Sub TxtDniForSearch_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TxtDniForSearch.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then

        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            If TxtDniForSearch.Text.Length = 8 Then
                DgvAttendances.DataSource = clsAsistencia.List(TxtDniForSearch.Text)
                DgvAttendances.Columns(0).HeaderText = "Id"
                DgvAttendances.Columns(1).HeaderText = "Fecha"
                DgvAttendances.Columns(2).HeaderText = "Hora de Entrada"
                DgvAttendances.Columns(3).HeaderText = "Hora de Salida"
                DgvAttendances.Columns(4).HeaderText = "DNI"
                DgvAttendances.Columns.Remove("Justification")
                DgvAttendances.Columns.Remove("Employee")

            Else
                MessageBox.Show("Ingrese un DNI de 8 digitos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub BtnRegisterSchedule_Click(sender As Object, e As EventArgs) Handles BtnRegisterSchedule.Click
        Try
            Dim jus As New capaDatos.justification
            SetNewJustification(jus)
            clsJustificacion.Register(jus)
            MessageBox.Show("Justificación registrada correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SetNewJustification(j As capaDatos.justification)
        j.fecha = DateTime.Now()
        j.reason = TxtReason.Text
        j.state = RbtState.Checked
        j.AssistanceId = Convert.ToInt32(DgvAttendances.CurrentRow.Cells(0).Value)
    End Sub

    Private Sub frmOprJustifacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub DgvAttendances_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvAttendances.CellContentClick

    End Sub
End Class