﻿Imports capaNegocio
Public Class frmOprLicencia
    Dim span As New TimeSpan
    Private Sub frmOprLicencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListAllEmployees(clsEmpleado.ListEmployee)
        SetLicensesTypeCombo()
        SetControls()
    End Sub
    Private Sub ListAllEmployees(List)
        Try
            DgvEmpl.DataSource = List
            DgvEmpl.AutoResizeColumn(0)
            DgvEmpl.AutoResizeColumn(1)
            DgvEmpl.AutoResizeColumn(2)
            DgvEmpl.AutoResizeColumn(4)
            DgvEmpl.AutoResizeColumn(6)
            DgvEmpl.AutoResizeColumn(7)
            DgvEmpl.Columns(0).HeaderText = "DNI"
            DgvEmpl.Columns(1).HeaderText = "Nombres"
            DgvEmpl.Columns(2).HeaderText = "Apellidos"
            DgvEmpl.Columns(3).HeaderText = "Género"
            DgvEmpl.Columns(4).HeaderText = "Dirección"
            DgvEmpl.Columns(5).HeaderText = "Celular"
            DgvEmpl.Columns(6).HeaderText = "Correo"
            DgvEmpl.Columns(7).HeaderText = "Estado"
            DgvEmpl.Columns.Remove("schedule")
            DgvEmpl.Columns.Remove("license")
            DgvEmpl.Columns.Remove("users")
            DgvEmpl.Columns.Remove("assistance")
            DgvEmpl.Columns.Remove("contract")
            DgvEmpl.Columns.Remove("permission")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub SetLicensesTypeCombo()
        Try
            CbxTypeLicense.DataSource = clsTipoLicencia.ListLicenseType()
            CbxTypeLicense.DisplayMember = "Description"
            CbxTypeLicense.ValueMember = "LicenseTypeId"
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SetControls()
        DtpEndDate.Value = DateTime.Now
        DtpStartDate.Value = DateTime.Now
        DtpPresentation.Value = DateTime.Now
        TxtDocument.Clear()

    End Sub

    Private Sub CbxTypeLicense_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CbxTypeLicense.SelectedIndexChanged

        Try
            'Dim type = CbxTypeLicense.SelectedItem.ToString()
            Dim value = Convert.ToInt32(CbxTypeLicense.SelectedValue.ToString())
                span = clsTipoLicencia.GetMaxTimeSpan(value)

            Catch ex As Exception

        End Try

    End Sub

    Private Sub CbxTypeLicense_SelectedValueChanged(sender As Object, e As EventArgs) Handles CbxTypeLicense.SelectedValueChanged

    End Sub

    Private Sub BtnRegisterLicense_Click(sender As Object, e As EventArgs) Handles BtnRegisterLicense.Click
        If DtpEndDate.Value - DtpStartDate.Value > span Then
            MessageBox.Show("El rango de la licencia excede a los dias permitidos, asegúrese de haber elegido un tipo correcto", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        ElseIf (DtpEndDate.Value - DtpStartDate.Value) < TimeSpan.FromDays(2) Then
            MessageBox.Show("El rango de fechas ingresado no debe ser menor a 2 dias", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else
            Dim new_license As New capaDatos.License
            SetNewLicense(new_license)
            clsLicencia.Register(new_license)
            MessageBox.Show("Registro exitoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            SetControls()
        End If


    End Sub
    Private Sub SetNewLicense(l As capaDatos.License)
        l.Dni = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)
        l.Document = TxtDocument.Text
        l.EndDate = DtpEndDate.Value
        l.StartDate = DtpStartDate.Value
        l.State = IIf(DateTime.Now > DtpStartDate.Value And DateTime.Now < DtpEndDate.Value, True, False)
        l.TypeId = Convert.ToInt32(CbxTypeLicense.SelectedValue.ToString())
        l.PresentationDate = DtpPresentation.Value
    End Sub

    Private Sub DgvEmpl_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvEmpl.CellClick

    End Sub

    Private Sub DgvEmpl_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvEmpl.CellContentClick

    End Sub
End Class