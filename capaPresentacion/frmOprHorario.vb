﻿Imports capaNegocio
Imports capaDatos
Public Class frmOprHorario
    Dim day As List(Of Object) = New List(Of Object)
    Private Sub frmOprHorario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListAllEmployees(clsEmpleado.ListEmployee)
    End Sub

    Private Sub ListAllEmployees(List)
        Try
            DgvEmpl.DataSource = List
            DgvEmpl.AutoResizeColumn(0)
            DgvEmpl.AutoResizeColumn(1)
            DgvEmpl.AutoResizeColumn(2)
            DgvEmpl.AutoResizeColumn(4)
            DgvEmpl.AutoResizeColumn(6)
            DgvEmpl.AutoResizeColumn(7)
            DgvEmpl.Columns(0).HeaderText = "DNI"
            DgvEmpl.Columns(1).HeaderText = "Nombres"
            DgvEmpl.Columns(2).HeaderText = "Apellidos"
            DgvEmpl.Columns(3).HeaderText = "Género"
            DgvEmpl.Columns(4).HeaderText = "Dirección"
            DgvEmpl.Columns(5).HeaderText = "Celular"
            DgvEmpl.Columns(6).HeaderText = "Correo"
            DgvEmpl.Columns(7).HeaderText = "Estado"
            DgvEmpl.Columns.Remove("schedule")
            DgvEmpl.Columns.Remove("license")
            DgvEmpl.Columns.Remove("users")
            DgvEmpl.Columns.Remove("assistance")
            DgvEmpl.Columns.Remove("contract")
            DgvEmpl.Columns.Remove("permission")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ListAllSchedule(list)
        Try
            DgvSchedule.Columns.Clear()
            DgvSchedule.DataSource = list
            DgvSchedule.Columns(0).HeaderText = "Id"
            DgvSchedule.Columns(1).HeaderText = "Fecha de Inicio"
            DgvSchedule.Columns(2).HeaderText = "Fecha de Salida"
            DgvSchedule.Columns(3).HeaderText = "Estado"
            DgvSchedule.Columns(4).HeaderText = "DNI"
            DgvSchedule.Columns.Remove("Employee")
            DgvSchedule.Columns.Remove("ScheduleDetail")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ListAllScheduleDetails(list)
        Try
            DgvScheduleDetail.DataSource = list
            DgvScheduleDetail.Columns(0).HeaderText = "Id"
            DgvScheduleDetail.Columns(1).HeaderText = "Horario Id"
            DgvScheduleDetail.Columns(2).HeaderText = "Día"
            DgvScheduleDetail.Columns(3).HeaderText = "Hora de Entrada"
            DgvScheduleDetail.Columns(4).HeaderText = "Hora de Salida"
            DgvScheduleDetail.Columns.Remove("Schedule")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub BtnRegisterSchedule_Click(sender As Object, e As EventArgs) Handles BtnRegisterSchedule.Click
        Try
            Dim span As TimeSpan = TimeSpan.FromDays(1)
            If DtpEndDate.Value - DtpStartDate.Value < span Or CbxDays.SelectedIndex = -1 Then
                MessageBox.Show("Ingrese los datos correctos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                Dim horas As Integer = 0
                For Each s In day
                    horas += Math.Abs(s(2) - s(1))
                Next
                If horas > 40 Then
                    MessageBox.Show("El horario excede las 40 horas", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    DgvDetails.Columns.Clear()
                    day = New List(Of Object)
                Else
                    Using DB = New BDAsistenciaEntities()
                        Using dbContextTransaction = DB.Database.BeginTransaction()
                            Try
                                Dim schedu As New capaDatos.schedule
                                SetNewSchedule(schedu)
                                Dim id_hor = clsHorario.Register(schedu, DB)
                                Dim sche_details As New capaDatos.scheduleDetail
                                For i = 0 To DgvDetails.Rows.Count - 1
                                    SetNewDetailSchedule(sche_details, day(i), id_hor)
                                    clsDetalleHorario.Register(sche_details, DB)
                                Next
                                RegisterAsistances(DB)
                                dbContextTransaction.Commit()
                                MessageBox.Show("Registro correcto", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                ClearControls()
                            Catch ex As Exception
                                dbContextTransaction.Rollback()
                                MessageBox.Show("Lo siento mucho, no te suicides", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End Try

                        End Using

                    End Using
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub SetNewSchedule(schedu)
        schedu.Dni = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)
        schedu.FinishDate = DtpEndDate.Value
        schedu.StartDate = DtpStartDate.Value
        schedu.State = RbtState.Checked
    End Sub
    Private Sub BlockScheduleControls()
        DtpEndDate.Enabled = False
        DtpStartDate.Enabled = False
        DgvEmpl.Enabled = False
        RbtState.Enabled = False
        BtnRegisterSchedule.Enabled = False
    End Sub
    Private Sub UnblockScheduleControls()
        DtpEndDate.Enabled = True
        DtpStartDate.Enabled = True
        DgvEmpl.Enabled = True
        RbtState.Enabled = True
        BtnRegisterSchedule.Enabled = True
    End Sub
    Private Sub BlockScheduleDetailsControls()
        CbxDays.Enabled = False
        NupEndHour.Enabled = False
        NupStartHour.Enabled = False
        BtnRegisterHours.Enabled = False
    End Sub
    Private Sub UnlockScheduleDetailsControls()
        CbxDays.Enabled = True
        NupEndHour.Enabled = True
        NupStartHour.Enabled = True
        BtnRegisterHours.Enabled = True
    End Sub

    Private Sub SetNewDetailSchedule(sche As capaDatos.ScheduleDetail, de As Object, id As Integer)
        sche.Day = de(0)
        sche.InHour = de(1)
        sche.OutHour = de(2)
        sche.ScheduleId = id
    End Sub

    Private Sub ClearControls()
        NupEndHour.ResetText()
        NupStartHour.ResetText()
        day = New List(Of Object)
        DgvDetails.Columns.Clear()
        CbxDays.SelectedIndex = -1
        DtpStartDate.Value = DateTime.Now
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BtnRegisterHours.Click

        If (CbxDays.SelectedIndex = -1) Then
            MessageBox.Show("Elija un dia", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim flag As Boolean = True
            For Each s In day
                If s(0) = CbxDays.SelectedIndex + 1 Then
                    flag = False
                End If
            Next
            If flag = True Then
                day.Add({CbxDays.SelectedIndex + 1, NupStartHour.Value, NupEndHour.Value})
                DgvDetails.Rows.Add(CbxDays.SelectedItem, NupStartHour.Value, NupEndHour.Value)
            Else
                MessageBox.Show("Este día ya ha sido seleccionado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        End If

    End Sub

    Private Sub TabControl1_Click(sender As Object, e As EventArgs) Handles TabControl1.Click
        Try
            ListAllSchedule(clsHorario.ListSchedule)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub TextBox5_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TxtDniForSearch.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            Try
                If TxtDniForSearch.TextLength > 0 Then
                    Dim schedule_list = clsHorario.SearchScheduleForDNI(TxtDniForSearch.Text)
                    ListAllSchedule(schedule_list)
                Else
                    ListAllSchedule(clsHorario.ListSchedule)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If
    End Sub

    Private Sub DgvSchedule_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvSchedule.CellContentClick


    End Sub

    Private Sub DgvScheduleDetail_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvScheduleDetail.CellContentClick

    End Sub

    Private Function GetDayByIndex(ind As Integer) As String
        If ind = 1 Then
            Return "Lunes"
        ElseIf ind = 2 Then
            Return "Martes"
        ElseIf ind = 3 Then
            Return "Miércoles"
        ElseIf ind = 4 Then
            Return "Jueves"
        ElseIf ind = 5 Then
            Return "Viernes"
        ElseIf ind = 6 Then
            Return "Sábado"
        Else
            Return "Domingo"
        End If
    End Function

    Private Sub BtnChangeSchedule_Click(sender As Object, e As EventArgs) Handles BtnChangeSchedule.Click
        Try
            Dim detail As New capaDatos.ScheduleDetail
            SetNewDetailForSchedule(detail)
            clsDetalleHorario.ModifyScheduleDetail(detail)
            MessageBox.Show("Cambio ejecutado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ClearModifyControls()
        Catch ex As Exception
            MessageBox.Show("No puede ser causa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ClearModifyControls()
        TxtDniForSearch.Clear()
        DgvScheduleDetail.Columns.Clear()
        TxtSelectedDay.Clear()
        NudNewInHour.Value = 0
        NudNewOutHour.Value = 0
        ListAllSchedule(clsHorario.ListSchedule)
    End Sub

    Private Sub SetNewDetailForSchedule(sched As capaDatos.ScheduleDetail)
        sched.Id = Convert.ToInt32(DgvScheduleDetail.CurrentRow.Cells(0).Value)
        sched.ScheduleId = Convert.ToInt32(DgvScheduleDetail.CurrentRow.Cells(1).Value)
        sched.Day = Convert.ToString(DgvScheduleDetail.CurrentRow.Cells(2).Value)
        sched.InHour = NudNewInHour.Value
        sched.OutHour = NudNewOutHour.Value
    End Sub
    Private Sub SetNewAssistance()
        'saco el numero del dia de la semana con el que comienza el horario
        Dim beg As Integer = Integer.Parse(DtpStartDate.Value.DayOfWeek())
        'seteo una variable date donde comienza en donde comienza mi horario y de ahi iremos hacia adelante, se usara para agregar fecha en asistencia
        Dim start_date As DateTime = DtpStartDate.Value
        'bandera en false para poder ejecutar el do until
        Dim flag As Boolean = False
        'comenzaremos un do until para ir por
        Do Until flag = True
            'ire por cada dia agregado en el array day y vere si el dia en formato entero es igual al dia de la semana con el que comienza el horario
            For Each i In day
                'si el el dia del array es igual al dia del horario entonces los agrego en asistencia
                If beg = day(i)(0) Then
                    Dim assis As New capaDatos.assistance
                    assis.dni = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)
                    assis.fecha = start_date
                    clsAsistencia.RegisterAssistance(assis)
                End If
            Next
            'si no es igual a ningun dia seguiremos
            'si la fecha de inicio es igual a la fecha fin entonces salimos del bucle, esto es para salir del bucle
            If start_date = DtpEndDate.Value Then
                Exit Do
            End If
            'si mi dia entero de inicio no es igual a ningun dia entero del array entonces le sumamos 1 para seguir evaluando en el otro do until
            beg += 1
            'tambien le sumamos un dia a la fecha de inicio ya que la primera fecha no cumplio con la condicion (esta se registra en la asistencia) 
            start_date = start_date.AddDays(1)
            'si el dia entero es 8 o mayor nunca coincidira con los dias del array days porque estos van de 1 a 7 asi que reseteo la variable en caso se haya pasado del limite 7
            If beg = 8 Then
                beg = 1
            End If
        Loop

    End Sub
    Private Sub RegisterAsistances(DB As BDAsistenciaEntities)
        Dim cantidad_dias_totales = DtpEndDate.Value - DtpStartDate.Value
        Dim i As Integer
        Dim dia_control = DtpStartDate.Value()
        Dim dia_entero As Integer = Integer.Parse(DtpStartDate.Value.DayOfWeek())
        For i = 0 To cantidad_dias_totales.Days
            For Each s In day
                If dia_entero = s(0) Then
                    Dim assis As New capaDatos.assistance
                    assis.dni = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)
                    assis.fecha = dia_control
                    clsAsistencia.RegisterAssistanceNulls(assis, DB)
                End If
            Next
            dia_control = dia_control.AddDays(1)
            dia_entero += 1
            If dia_entero = 8 Then
                dia_entero = 1
            End If
        Next
    End Sub

    Private Sub DgvSchedule_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvSchedule.CellClick
        If e.RowIndex >= 0 Then
            Try
                Dim id_schedule = Convert.ToInt32(DgvSchedule.CurrentRow.Cells(0).Value)
                ListAllScheduleDetails(clsDetalleHorario.SearchDetailForScheduleId(id_schedule))
            Catch ex As Exception
                MessageBox.Show("Algo fue mal mostrando los detalles", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            MessageBox.Show("Esta fila no puede ser seleccionada", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub DgvScheduleDetail_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvScheduleDetail.CellClick
        If e.RowIndex >= 0 Then
            Try
                Dim id_day As New Integer
                id_day = Convert.ToInt32(DgvScheduleDetail.CurrentRow.Cells(2).Value)
                TxtSelectedDay.Text = GetDayByIndex(id_day)
            Catch ex As Exception
                MessageBox.Show("Flag", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            MessageBox.Show("Esta fila no puede ser seleccionada", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub DgvEmpl_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvEmpl.CellContentClick

    End Sub
End Class