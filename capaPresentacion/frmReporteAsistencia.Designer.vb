﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmReporteAsistencia
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReporteAsistencia))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnBuscarFecha = New System.Windows.Forms.Button()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DgvAsistencia = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtDni = New System.Windows.Forms.TextBox()
        Me.BtnBuscarDNI = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.DgvAsistencia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.btnBuscarFecha)
        Me.Panel1.Controls.Add(Me.dtpFecha)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.DgvAsistencia)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.TxtDni)
        Me.Panel1.Controls.Add(Me.BtnBuscarDNI)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(1, -5)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(840, 656)
        Me.Panel1.TabIndex = 1
        '
        'btnBuscarFecha
        '
        Me.btnBuscarFecha.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.btnBuscarFecha.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBuscarFecha.FlatAppearance.BorderSize = 0
        Me.btnBuscarFecha.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscarFecha.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscarFecha.ForeColor = System.Drawing.Color.White
        Me.btnBuscarFecha.Location = New System.Drawing.Point(555, 182)
        Me.btnBuscarFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBuscarFecha.Name = "btnBuscarFecha"
        Me.btnBuscarFecha.Size = New System.Drawing.Size(192, 33)
        Me.btnBuscarFecha.TabIndex = 18
        Me.btnBuscarFecha.Text = "Buscar"
        Me.btnBuscarFecha.UseVisualStyleBackColor = False
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(179, 185)
        Me.dtpFecha.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(333, 22)
        Me.dtpFecha.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(95, 188)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 23)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Fecha :"
        '
        'DgvAsistencia
        '
        Me.DgvAsistencia.AllowUserToAddRows = False
        Me.DgvAsistencia.AllowUserToDeleteRows = False
        Me.DgvAsistencia.AllowUserToOrderColumns = True
        Me.DgvAsistencia.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvAsistencia.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvAsistencia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvAsistencia.Location = New System.Drawing.Point(63, 260)
        Me.DgvAsistencia.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DgvAsistencia.Name = "DgvAsistencia"
        Me.DgvAsistencia.ReadOnly = True
        Me.DgvAsistencia.RowHeadersWidth = 51
        Me.DgvAsistencia.Size = New System.Drawing.Size(712, 341)
        Me.DgvAsistencia.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(109, 135)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 23)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "DNI :"
        '
        'TxtDni
        '
        Me.TxtDni.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtDni.Location = New System.Drawing.Point(179, 132)
        Me.TxtDni.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.TxtDni.Name = "TxtDni"
        Me.TxtDni.Size = New System.Drawing.Size(333, 29)
        Me.TxtDni.TabIndex = 13
        '
        'BtnBuscarDNI
        '
        Me.BtnBuscarDNI.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnBuscarDNI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnBuscarDNI.FlatAppearance.BorderSize = 0
        Me.BtnBuscarDNI.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnBuscarDNI.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBuscarDNI.ForeColor = System.Drawing.Color.White
        Me.BtnBuscarDNI.Location = New System.Drawing.Point(555, 132)
        Me.BtnBuscarDNI.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BtnBuscarDNI.Name = "BtnBuscarDNI"
        Me.BtnBuscarDNI.Size = New System.Drawing.Size(192, 33)
        Me.BtnBuscarDNI.TabIndex = 10
        Me.BtnBuscarDNI.Text = "Buscar"
        Me.BtnBuscarDNI.UseVisualStyleBackColor = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(32, 37)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(377, 46)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Consulta de Asistencia"
        '
        'frmReporteAsistencia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(836, 634)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(854, 681)
        Me.MinimizeBox = False
        Me.Name = "frmReporteAsistencia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Reporte de Asistencia"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DgvAsistencia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents BtnBuscarDNI As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents TxtDni As TextBox
    Friend WithEvents DgvAsistencia As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents btnBuscarFecha As Button
    Friend WithEvents dtpFecha As DateTimePicker
End Class
