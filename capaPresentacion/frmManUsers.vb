﻿Imports capaNegocio

Public Class frmManUsers

    Private user As capaDatos.Users
    Dim table As New DataTable
    Dim countE As Integer


    Private Sub SetNewUsers()
        user = New capaDatos.Users
        user.USerName = txtNombre.Text
        user.UserPassword = txtContraseña.Text
        user.UserState = rbEstado.Checked
        user.Dni = txtDni.Text
    End Sub

    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        If BtnRegister.Text = "Modificar" Then
            Try
                clsUsuario.Update(Integer.Parse(txtIdBuscar.Text), txtNombre.Text, txtContraseña.Text, txtDni.Text, rbEstado.Checked)
                BtnRegister.Text = "Registrar"
                ClearControls()
                ListTable()
                MessageBox.Show("Modificación exitosa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show("Error en la modificacion", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            Try
                countE = clsEmpleado.FindDni(txtDni.Text)
                If (txtNombre.TextLength = 0 Or txtContraseña.TextLength = 0 Or txtDni.TextLength = 0) Then
                    MessageBox.Show("Es necesario completar todos los campos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    ClearControls()
                ElseIf txtDni.TextLength = 8 And countE = 1 Then
                    SetNewUsers()
                    clsUsuario.RegisterUser(user)
                    ClearControls()
                    ListTable()
                    MessageBox.Show("Registro exitoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("El DNI no ha sido ingresado correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Catch ex As Exception
                MessageBox.Show("Error al registrar" + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
        End If

    End Sub


    Private Sub ClearControls()
        txtNombre.Clear()
        txtContraseña.Clear()
        txtDni.Clear()
    End Sub


    Private Sub AddButtonColumn(text As String)
        Dim bt As New DataGridViewButtonColumn With {
            .Text = text,
            .Name = "Btn" & text,
            .UseColumnTextForButtonValue = True,
            .FlatStyle = FlatStyle.Flat
        }
        bt.CellTemplate.Style.ForeColor = Color.Black
        dgwUsuario.Columns.Add(bt)
    End Sub

    Private Sub ListTable()
        Try
            dgwUsuario.Columns.Clear()
            dgwUsuario.DataSource = clsUsuario.ListUser()
            dgwUsuario.Columns.Remove("Employee")
            AddButtonColumn("Eliminar")
            AddButtonColumn("Desactivar")
            AddButtonColumn("Modificar")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub

    Private Sub ListUserByDNI()
        Try
            dgwUsuario.Columns.Clear()
            dgwUsuario.DataSource = clsUsuario.ListUserByDni(txtDNIBuscar.Text)
            dgwUsuario.Columns(0).HeaderText = "Id"
            dgwUsuario.Columns(1).HeaderText = "Usuario"
            dgwUsuario.Columns(2).HeaderText = "Contraseña"
            dgwUsuario.Columns(3).HeaderText = "DNI"
            dgwUsuario.Columns(4).HeaderText = "Estado"
            dgwUsuario.Columns.Remove("Employee")
            AddButtonColumn("Eliminar")
            AddButtonColumn("Desactivar")
            AddButtonColumn("Modificar")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmManUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtIdBuscar.Enabled = False
        ListTable()

    End Sub


    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click

        Try
            If (txtDNIBuscar.Text.Length > 0) Then

                ListUserByDNI()

            End If

        Catch ex As Exception
            MessageBox.Show("Error al buscar: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub UpdateUser()
        Try
            If txtIdBuscar.TextLength > 0 Then

                Dim c = clsUsuario.FindUser(Integer.Parse(txtIdBuscar.Text))
                txtNombre.Text = c.USerName
                txtDni.Text = c.Dni
                txtContraseña.Text = c.UserPassword
                rbEstado.Checked = c.UserState
                BtnRegister.Text = "Modificar"
            End If
        Catch ex As Exception
            MessageBox.Show("Error al modificar: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub Delete()
        Try
            If (txtIdBuscar.Text.Length > 0) Then
                clsUsuario.Delete(Integer.Parse(txtIdBuscar.Text))
                ClearControls()
                ListTable()
                MessageBox.Show("Eliminación exitosa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("No se encuentra un empleado con este id")
            End If
        Catch ex As Exception
            MessageBox.Show("Error al eliminar: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Down()
        Try
            If (txtIdBuscar.Text.Length > 0) Then
                clsUsuario.Down(Integer.Parse(txtIdBuscar.Text))
                ClearControls()
                ListTable()
                MessageBox.Show("Usuario desactivado exitosamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Error al desactivar usuario: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgwUsuario_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgwUsuario.CellClick
        txtIdBuscar.Text = Convert.ToString(dgwUsuario.CurrentRow.Cells(0).Value)
        Dim senderGrid = DirectCast(sender, DataGridView)
        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            If e.ColumnIndex = 5 Then
                Delete()
            ElseIf e.ColumnIndex = 6 Then
                Down()
            ElseIf e.ColumnIndex = 7 Then
                UpdateUser()
            End If
        End If
    End Sub
End Class