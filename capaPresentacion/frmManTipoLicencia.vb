﻿Imports capaNegocio
Public Class frmManTipoLicencia
    Dim id_typelicense As Integer
    Private Sub frmManTipoLicencia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListAllLicencesType(clsTipoLicencia.ListLicenseType())
        LockControls()
    End Sub

    Private Sub ListAllLicencesType(list As List(Of capaDatos.LicenseType))
        Try
            DgvTypeLicense.DataSource = list
            DgvTypeLicense.Columns(0).HeaderText = "Id"
            DgvTypeLicense.Columns(1).HeaderText = "Descripción"
            DgvTypeLicense.Columns(2).HeaderText = "Días Máximos"
            DgvTypeLicense.Columns.Remove("License")
            DgvTypeLicense.AutoResizeColumns()
        Catch ex As Exception
            MessageBox.Show("lo siento")
        End Try

    End Sub

    Private Sub LockControls()
        BtnModify.Enabled = False
    End Sub
    Private Sub UnlockControls()
        BtnModify.Enabled = True
    End Sub

    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        If BtnRegister.Text = "Registrar" Then
            Try
                Dim new_type As New capaDatos.LicenseType
                SetNewLicenseType(new_type)
                clsTipoLicencia.Register(new_type)
                MessageBox.Show("Registro exitoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ListAllLicencesType(clsTipoLicencia.ListLicenseType)
                ClearControls()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        ElseIf BtnRegister.Text = "Modificar" Then
            Try
                Dim new_type As New capaDatos.LicenseType
                SetDataForModify(new_type)
                clsTipoLicencia.ModifyLicenseType(new_type)
                MessageBox.Show("Modificación exitosa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ListAllLicencesType(clsTipoLicencia.ListLicenseType)
                ClearControls()
            Catch ex As Exception

            End Try
        End If


    End Sub
    Private Sub SetNewLicenseType(l As capaDatos.LicenseType)
        l.Description = TxtDescription.Text
        l.maxDias = NudMaxDays.Value
    End Sub
    Private Sub ClearControls()
        TxtDescription.Clear()
        NudMaxDays.ResetText()
        id_typelicense = Nothing
        BtnRegister.Text = "Registrar"
        BtnModify.Enabled = False
    End Sub

    Private Sub DgvTypeLicense_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvTypeLicense.CellClick
        If e.RowIndex >= 0 Then
            Try
                UnlockControls()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub
    Private Sub BtnModify_Click(sender As Object, e As EventArgs) Handles BtnModify.Click
        id_typelicense = Convert.ToInt32(DgvTypeLicense.CurrentRow.Cells(0).Value)
        TxtDescription.Text = Convert.ToString(DgvTypeLicense.CurrentRow.Cells(1).Value)
        NudMaxDays.Value = Convert.ToInt32(DgvTypeLicense.CurrentRow.Cells(2).Value)
        BtnRegister.Text = "Modificar"
    End Sub
    Private Sub SetDataForModify(l As capaDatos.LicenseType)
        l.Description = TxtDescription.Text
        l.maxDias = NudMaxDays.Value
        l.LicenseTypeId = id_typelicense
    End Sub

    Private Sub DgvTypeLicense_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DgvTypeLicense.CellContentClick

    End Sub
End Class