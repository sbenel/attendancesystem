﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOprHorario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOprHorario))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TbpNewSchedule = New System.Windows.Forms.TabPage()
        Me.DgvDetails = New System.Windows.Forms.DataGridView()
        Me.dia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.inhour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.outhour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BtnRegisterHours = New System.Windows.Forms.Button()
        Me.CbxDays = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.NupEndHour = New System.Windows.Forms.NumericUpDown()
        Me.NupStartHour = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.BtnRegisterSchedule = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DgvEmpl = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RbtState = New System.Windows.Forms.RadioButton()
        Me.DtpEndDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.NudNewOutHour = New System.Windows.Forms.NumericUpDown()
        Me.NudNewInHour = New System.Windows.Forms.NumericUpDown()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.DgvScheduleDetail = New System.Windows.Forms.DataGridView()
        Me.DgvSchedule = New System.Windows.Forms.DataGridView()
        Me.TxtSelectedDay = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.TxtDniForSearch = New System.Windows.Forms.TextBox()
        Me.BtnChangeSchedule = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TbpNewSchedule.SuspendLayout()
        CType(Me.DgvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NupEndHour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NupStartHour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvEmpl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.NudNewOutHour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NudNewInHour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvScheduleDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvSchedule, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(908, 875)
        Me.Panel1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TbpNewSchedule)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(-4, 85)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(912, 902)
        Me.TabControl1.TabIndex = 20
        '
        'TbpNewSchedule
        '
        Me.TbpNewSchedule.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.TbpNewSchedule.Controls.Add(Me.DgvDetails)
        Me.TbpNewSchedule.Controls.Add(Me.BtnRegisterHours)
        Me.TbpNewSchedule.Controls.Add(Me.CbxDays)
        Me.TbpNewSchedule.Controls.Add(Me.Label24)
        Me.TbpNewSchedule.Controls.Add(Me.Label20)
        Me.TbpNewSchedule.Controls.Add(Me.NupEndHour)
        Me.TbpNewSchedule.Controls.Add(Me.NupStartHour)
        Me.TbpNewSchedule.Controls.Add(Me.Label11)
        Me.TbpNewSchedule.Controls.Add(Me.BtnRegisterSchedule)
        Me.TbpNewSchedule.Controls.Add(Me.Label9)
        Me.TbpNewSchedule.Controls.Add(Me.Label8)
        Me.TbpNewSchedule.Controls.Add(Me.Label6)
        Me.TbpNewSchedule.Controls.Add(Me.Label5)
        Me.TbpNewSchedule.Controls.Add(Me.DgvEmpl)
        Me.TbpNewSchedule.Controls.Add(Me.Label4)
        Me.TbpNewSchedule.Controls.Add(Me.Label3)
        Me.TbpNewSchedule.Controls.Add(Me.RbtState)
        Me.TbpNewSchedule.Controls.Add(Me.DtpEndDate)
        Me.TbpNewSchedule.Controls.Add(Me.Label2)
        Me.TbpNewSchedule.Controls.Add(Me.DtpStartDate)
        Me.TbpNewSchedule.Controls.Add(Me.Label1)
        Me.TbpNewSchedule.Location = New System.Drawing.Point(4, 24)
        Me.TbpNewSchedule.Name = "TbpNewSchedule"
        Me.TbpNewSchedule.Padding = New System.Windows.Forms.Padding(3)
        Me.TbpNewSchedule.Size = New System.Drawing.Size(904, 874)
        Me.TbpNewSchedule.TabIndex = 0
        Me.TbpNewSchedule.Text = "Agregar nuevo horario"
        '
        'DgvDetails
        '
        Me.DgvDetails.AllowUserToAddRows = False
        Me.DgvDetails.AllowUserToDeleteRows = False
        Me.DgvDetails.AllowUserToOrderColumns = True
        Me.DgvDetails.AllowUserToResizeColumns = False
        Me.DgvDetails.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvDetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvDetails.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvDetails.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dia, Me.inhour, Me.outhour})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvDetails.DefaultCellStyle = DataGridViewCellStyle2
        Me.DgvDetails.EnableHeadersVisualStyles = False
        Me.DgvDetails.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvDetails.Location = New System.Drawing.Point(71, 502)
        Me.DgvDetails.Name = "DgvDetails"
        Me.DgvDetails.ReadOnly = True
        Me.DgvDetails.RowHeadersVisible = False
        Me.DgvDetails.RowHeadersWidth = 51
        Me.DgvDetails.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvDetails.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvDetails.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvDetails.Size = New System.Drawing.Size(604, 116)
        Me.DgvDetails.TabIndex = 51
        '
        'dia
        '
        Me.dia.HeaderText = "Día"
        Me.dia.MinimumWidth = 6
        Me.dia.Name = "dia"
        Me.dia.ReadOnly = True
        Me.dia.Width = 200
        '
        'inhour
        '
        Me.inhour.HeaderText = "Hora de entrada"
        Me.inhour.MinimumWidth = 6
        Me.inhour.Name = "inhour"
        Me.inhour.ReadOnly = True
        Me.inhour.Width = 200
        '
        'outhour
        '
        Me.outhour.HeaderText = "Hora de salida"
        Me.outhour.MinimumWidth = 6
        Me.outhour.Name = "outhour"
        Me.outhour.ReadOnly = True
        Me.outhour.Width = 200
        '
        'BtnRegisterHours
        '
        Me.BtnRegisterHours.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnRegisterHours.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRegisterHours.FlatAppearance.BorderSize = 0
        Me.BtnRegisterHours.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRegisterHours.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRegisterHours.ForeColor = System.Drawing.Color.White
        Me.BtnRegisterHours.Location = New System.Drawing.Point(724, 433)
        Me.BtnRegisterHours.Name = "BtnRegisterHours"
        Me.BtnRegisterHours.Size = New System.Drawing.Size(124, 40)
        Me.BtnRegisterHours.TabIndex = 50
        Me.BtnRegisterHours.Text = "Agregar día"
        Me.BtnRegisterHours.UseVisualStyleBackColor = False
        '
        'CbxDays
        '
        Me.CbxDays.FormattingEnabled = True
        Me.CbxDays.Items.AddRange(New Object() {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"})
        Me.CbxDays.Location = New System.Drawing.Point(102, 427)
        Me.CbxDays.Name = "CbxDays"
        Me.CbxDays.Size = New System.Drawing.Size(121, 23)
        Me.CbxDays.TabIndex = 47
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(627, 434)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(15, 17)
        Me.Label24.TabIndex = 46
        Me.Label24.Text = "h"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(424, 433)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(15, 17)
        Me.Label20.TabIndex = 43
        Me.Label20.Text = "h"
        '
        'NupEndHour
        '
        Me.NupEndHour.Location = New System.Drawing.Point(566, 428)
        Me.NupEndHour.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.NupEndHour.Name = "NupEndHour"
        Me.NupEndHour.Size = New System.Drawing.Size(59, 23)
        Me.NupEndHour.TabIndex = 40
        '
        'NupStartHour
        '
        Me.NupStartHour.Location = New System.Drawing.Point(364, 428)
        Me.NupStartHour.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.NupStartHour.Name = "NupStartHour"
        Me.NupStartHour.Size = New System.Drawing.Size(59, 23)
        Me.NupStartHour.TabIndex = 39
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(60, 427)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 17)
        Me.Label11.TabIndex = 37
        Me.Label11.Text = "Días :"
        '
        'BtnRegisterSchedule
        '
        Me.BtnRegisterSchedule.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnRegisterSchedule.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRegisterSchedule.FlatAppearance.BorderSize = 0
        Me.BtnRegisterSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRegisterSchedule.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRegisterSchedule.ForeColor = System.Drawing.Color.White
        Me.BtnRegisterSchedule.Location = New System.Drawing.Point(724, 492)
        Me.BtnRegisterSchedule.Name = "BtnRegisterSchedule"
        Me.BtnRegisterSchedule.Size = New System.Drawing.Size(124, 40)
        Me.BtnRegisterSchedule.TabIndex = 36
        Me.BtnRegisterSchedule.Text = "Guardar horario"
        Me.BtnRegisterSchedule.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(462, 428)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 17)
        Me.Label9.TabIndex = 32
        Me.Label9.Text = "Hora de salida :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(250, 428)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(111, 17)
        Me.Label8.TabIndex = 30
        Me.Label8.Text = "Hora de ingreso :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(33, 374)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(200, 21)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Registro de horas por día"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(51, 140)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 17)
        Me.Label5.TabIndex = 28
        Me.Label5.Text = "Elegir al empleado"
        '
        'DgvEmpl
        '
        Me.DgvEmpl.AllowUserToAddRows = False
        Me.DgvEmpl.AllowUserToDeleteRows = False
        Me.DgvEmpl.AllowUserToOrderColumns = True
        Me.DgvEmpl.AllowUserToResizeColumns = False
        Me.DgvEmpl.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvEmpl.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvEmpl.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvEmpl.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvEmpl.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DgvEmpl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvEmpl.DefaultCellStyle = DataGridViewCellStyle4
        Me.DgvEmpl.EnableHeadersVisualStyles = False
        Me.DgvEmpl.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvEmpl.Location = New System.Drawing.Point(54, 167)
        Me.DgvEmpl.Name = "DgvEmpl"
        Me.DgvEmpl.ReadOnly = True
        Me.DgvEmpl.RowHeadersVisible = False
        Me.DgvEmpl.RowHeadersWidth = 51
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvEmpl.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvEmpl.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvEmpl.Size = New System.Drawing.Size(732, 177)
        Me.DgvEmpl.TabIndex = 27
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(33, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 21)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Datos generales"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(539, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 17)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Estado:"
        '
        'RbtState
        '
        Me.RbtState.AutoSize = True
        Me.RbtState.Checked = True
        Me.RbtState.Location = New System.Drawing.Point(603, 66)
        Me.RbtState.Name = "RbtState"
        Me.RbtState.Size = New System.Drawing.Size(59, 19)
        Me.RbtState.TabIndex = 24
        Me.RbtState.TabStop = True
        Me.RbtState.Text = "Activo"
        Me.RbtState.UseVisualStyleBackColor = True
        '
        'DtpEndDate
        '
        Me.DtpEndDate.Location = New System.Drawing.Point(289, 100)
        Me.DtpEndDate.Name = "DtpEndDate"
        Me.DtpEndDate.Size = New System.Drawing.Size(224, 23)
        Me.DtpEndDate.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(165, 100)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 17)
        Me.Label2.TabIndex = 22
        Me.Label2.Text = "Fecha de inicio"
        '
        'DtpStartDate
        '
        Me.DtpStartDate.Location = New System.Drawing.Point(289, 60)
        Me.DtpStartDate.Name = "DtpStartDate"
        Me.DtpStartDate.Size = New System.Drawing.Size(224, 23)
        Me.DtpStartDate.TabIndex = 21
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(165, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(94, 17)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Fecha de inicio"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.NudNewOutHour)
        Me.TabPage2.Controls.Add(Me.NudNewInHour)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.DgvScheduleDetail)
        Me.TabPage2.Controls.Add(Me.DgvSchedule)
        Me.TabPage2.Controls.Add(Me.TxtSelectedDay)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.TxtDniForSearch)
        Me.TabPage2.Controls.Add(Me.BtnChangeSchedule)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(904, 874)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Modificar un horario"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(606, 612)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(15, 17)
        Me.Label10.TabIndex = 70
        Me.Label10.Text = "h"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(403, 611)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(15, 17)
        Me.Label14.TabIndex = 69
        Me.Label14.Text = "h"
        '
        'NudNewOutHour
        '
        Me.NudNewOutHour.Location = New System.Drawing.Point(545, 606)
        Me.NudNewOutHour.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.NudNewOutHour.Name = "NudNewOutHour"
        Me.NudNewOutHour.Size = New System.Drawing.Size(59, 23)
        Me.NudNewOutHour.TabIndex = 68
        '
        'NudNewInHour
        '
        Me.NudNewInHour.Location = New System.Drawing.Point(343, 606)
        Me.NudNewInHour.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.NudNewInHour.Name = "NudNewInHour"
        Me.NudNewInHour.Size = New System.Drawing.Size(59, 23)
        Me.NudNewInHour.TabIndex = 67
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(441, 606)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(101, 17)
        Me.Label15.TabIndex = 66
        Me.Label15.Text = "Hora de salida :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(229, 606)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(111, 17)
        Me.Label22.TabIndex = 65
        Me.Label22.Text = "Hora de ingreso :"
        '
        'DgvScheduleDetail
        '
        Me.DgvScheduleDetail.AllowUserToAddRows = False
        Me.DgvScheduleDetail.AllowUserToDeleteRows = False
        Me.DgvScheduleDetail.AllowUserToOrderColumns = True
        Me.DgvScheduleDetail.AllowUserToResizeColumns = False
        Me.DgvScheduleDetail.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvScheduleDetail.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvScheduleDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvScheduleDetail.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvScheduleDetail.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DgvScheduleDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvScheduleDetail.DefaultCellStyle = DataGridViewCellStyle6
        Me.DgvScheduleDetail.EnableHeadersVisualStyles = False
        Me.DgvScheduleDetail.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvScheduleDetail.Location = New System.Drawing.Point(172, 332)
        Me.DgvScheduleDetail.Name = "DgvScheduleDetail"
        Me.DgvScheduleDetail.ReadOnly = True
        Me.DgvScheduleDetail.RowHeadersVisible = False
        Me.DgvScheduleDetail.RowHeadersWidth = 51
        Me.DgvScheduleDetail.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvScheduleDetail.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvScheduleDetail.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvScheduleDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvScheduleDetail.Size = New System.Drawing.Size(495, 120)
        Me.DgvScheduleDetail.TabIndex = 64
        '
        'DgvSchedule
        '
        Me.DgvSchedule.AllowUserToAddRows = False
        Me.DgvSchedule.AllowUserToDeleteRows = False
        Me.DgvSchedule.AllowUserToOrderColumns = True
        Me.DgvSchedule.AllowUserToResizeColumns = False
        Me.DgvSchedule.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvSchedule.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvSchedule.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvSchedule.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvSchedule.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.DgvSchedule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvSchedule.DefaultCellStyle = DataGridViewCellStyle8
        Me.DgvSchedule.EnableHeadersVisualStyles = False
        Me.DgvSchedule.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvSchedule.Location = New System.Drawing.Point(172, 130)
        Me.DgvSchedule.Name = "DgvSchedule"
        Me.DgvSchedule.ReadOnly = True
        Me.DgvSchedule.RowHeadersVisible = False
        Me.DgvSchedule.RowHeadersWidth = 51
        Me.DgvSchedule.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvSchedule.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvSchedule.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvSchedule.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvSchedule.Size = New System.Drawing.Size(495, 120)
        Me.DgvSchedule.TabIndex = 63
        '
        'TxtSelectedDay
        '
        Me.TxtSelectedDay.Location = New System.Drawing.Point(191, 534)
        Me.TxtSelectedDay.Name = "TxtSelectedDay"
        Me.TxtSelectedDay.ReadOnly = True
        Me.TxtSelectedDay.Size = New System.Drawing.Size(100, 23)
        Me.TxtSelectedDay.TabIndex = 62
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(78, 535)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(107, 17)
        Me.Label19.TabIndex = 61
        Me.Label19.Text = "Día seleccionado"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(53, 306)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(95, 17)
        Me.Label12.TabIndex = 60
        Me.Label12.Text = "Detalle por día"
        '
        'TxtDniForSearch
        '
        Me.TxtDniForSearch.Location = New System.Drawing.Point(239, 62)
        Me.TxtDniForSearch.Name = "TxtDniForSearch"
        Me.TxtDniForSearch.Size = New System.Drawing.Size(547, 23)
        Me.TxtDniForSearch.TabIndex = 58
        '
        'BtnChangeSchedule
        '
        Me.BtnChangeSchedule.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnChangeSchedule.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnChangeSchedule.FlatAppearance.BorderSize = 0
        Me.BtnChangeSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnChangeSchedule.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnChangeSchedule.ForeColor = System.Drawing.Color.White
        Me.BtnChangeSchedule.Location = New System.Drawing.Point(654, 677)
        Me.BtnChangeSchedule.Name = "BtnChangeSchedule"
        Me.BtnChangeSchedule.Size = New System.Drawing.Size(132, 32)
        Me.BtnChangeSchedule.TabIndex = 57
        Me.BtnChangeSchedule.Text = "Ejecutar cambio"
        Me.BtnChangeSchedule.UseVisualStyleBackColor = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(53, 480)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(152, 17)
        Me.Label13.TabIndex = 53
        Me.Label13.Text = "Ingrese las nuevas horas"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(35, 272)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(160, 21)
        Me.Label16.TabIndex = 48
        Me.Label16.Text = "Detalles del horario"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(53, 62)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(179, 17)
        Me.Label17.TabIndex = 47
        Me.Label17.Text = "Buscar por DNI de empleado"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(35, 30)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(132, 21)
        Me.Label18.TabIndex = 45
        Me.Label18.Text = "Datos generales"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(53, 102)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(200, 17)
        Me.Label21.TabIndex = 39
        Me.Label21.Text = "Seleccione el horario a modificar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(30, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(452, 37)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Administración de horario laboral"
        '
        'frmOprHorario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 749)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOprHorario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Horario"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TbpNewSchedule.ResumeLayout(False)
        Me.TbpNewSchedule.PerformLayout()
        CType(Me.DgvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NupEndHour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NupStartHour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvEmpl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.NudNewOutHour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NudNewInHour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvScheduleDetail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvSchedule, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label7 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TbpNewSchedule As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label11 As Label
    Friend WithEvents BtnRegisterSchedule As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents DgvEmpl As DataGridView
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents RbtState As RadioButton
    Friend WithEvents DtpEndDate As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents DtpStartDate As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents TxtSelectedDay As TextBox
    Friend WithEvents Label19 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents TxtDniForSearch As TextBox
    Public WithEvents BtnChangeSchedule As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents NupEndHour As NumericUpDown
    Friend WithEvents NupStartHour As NumericUpDown
    Friend WithEvents BtnRegisterHours As Button
    Friend WithEvents CbxDays As ComboBox
    Friend WithEvents DgvDetails As DataGridView
    Friend WithEvents dia As DataGridViewTextBoxColumn
    Friend WithEvents inhour As DataGridViewTextBoxColumn
    Friend WithEvents outhour As DataGridViewTextBoxColumn
    Friend WithEvents DgvSchedule As DataGridView
    Friend WithEvents DgvScheduleDetail As DataGridView
    Friend WithEvents Label10 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents NudNewOutHour As NumericUpDown
    Friend WithEvents NudNewInHour As NumericUpDown
    Friend WithEvents Label15 As Label
    Friend WithEvents Label22 As Label
End Class
