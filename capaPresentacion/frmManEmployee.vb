﻿Imports capaNegocio
Public Class frmManEmployee
    Private employ As New capaDatos.Employee

    Private Sub Label2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        If CbxSex.SelectedItem IsNot Nothing Then
            Try
                If (BtnRegister.Text = "Registrar") Then
                    SetNewEmployeer(employ)
                    clsEmpleado.Register(employ)
                    ListAllEmployees(clsEmpleado.ListEmployee())
                    ClearControls()
                ElseIf (BtnRegister.Text = "Modificar") Then
                    Dim new_employ As New capaDatos.employee
                    SetNewEmployeer(new_employ)
                    clsEmpleado.ModifyEmployee(new_employ)
                    ListAllEmployees(clsEmpleado.ListEmployee())
                    ClearControls()
                    BtnRegister.Text = "Register"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        Else
            MessageBox.Show("Seleccione un sexo correcto", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Private Sub SetNewEmployeer(employee As capaDatos.Employee)
        Try
            employee.Dni = TxtDni.Text
            employee.Name = TxtNombre.Text
            employee.Lastname = TxtLastname.Text
            employee.Address = TxtAdress.Text
            employee.Genre = CbxSex.SelectedItem.ToString.Chars(0)
            employee.Phone = TxtPhone.Text
            employee.Email = TxtEmail.Text
            employee.State = RbtnState.Checked
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub frmManEmployee_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListAllEmployees(clsEmpleado.ListEmployee())
    End Sub

    Private Sub ClearControls()
        For Each c In SpcEmployee.Panel1.Controls
            If TypeOf c Is TextBox Then
                c.Clear()
            End If
        Next
        CbxSex.ResetText()
    End Sub
    Private Sub ListAllEmployees(list)
        Try
            DgvEmpl.Columns.Clear()
            DgvEmpl.DataSource = list
            DgvEmpl.Columns(0).HeaderText = "DNI"
            DgvEmpl.Columns(1).HeaderText = "Nombres"
            DgvEmpl.Columns(2).HeaderText = "Apellidos"
            DgvEmpl.Columns(3).HeaderText = "Género"
            DgvEmpl.Columns(4).HeaderText = "Dirección"
            DgvEmpl.Columns(5).HeaderText = "Celular"
            DgvEmpl.Columns(6).HeaderText = "Correo"
            DgvEmpl.Columns(7).HeaderText = "Estado"
            DgvEmpl.AutoResizeColumn(0)
            DgvEmpl.AutoResizeColumn(1)
            DgvEmpl.AutoResizeColumn(2)
            DgvEmpl.AutoResizeColumn(4)
            DgvEmpl.AutoResizeColumn(6)
            DgvEmpl.AutoResizeColumn(7)
            DgvEmpl.Columns.Remove("schedule")
            DgvEmpl.Columns.Remove("license")
            DgvEmpl.Columns.Remove("users")
            DgvEmpl.Columns.Remove("assistance")
            DgvEmpl.Columns.Remove("contract")
            DgvEmpl.Columns.Remove("permission")
            AddButtonColumn("Editar")
            AddButtonColumn("Borrar")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AddButtonColumn(text As String)
        Dim bt As New DataGridViewButtonColumn With {
            .Text = text,
            .Name = text,
            .UseColumnTextForButtonValue = True,
            .FlatStyle = FlatStyle.Flat
        }
        bt.CellTemplate.Style.ForeColor = Color.Black
        DgvEmpl.Columns.Add(bt)
    End Sub
    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As DataGridViewCellEventArgs) Handles DgvEmpl.CellClick

        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then

            Dim dni As String = DgvEmpl.CurrentRow.Cells(0).Value.ToString()

            If e.ColumnIndex = 8 Then
                Try
                    SetDataForEditing()
                Catch ex As Exception
                    MessageBox.Show("No se pudo listar al empleado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            ElseIf e.ColumnIndex = 9 Then
                Try
                    'Dim dni = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)

                    Dim em As Boolean = clsEmpleado.Validation(dni)

                    If (em = False) Then
                        clsEmpleado.DeleteEmployee(dni)
                        DgvEmpl.Refresh()
                        MessageBox.Show("Empleado eliminado correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ListAllEmployees(clsEmpleado.ListEmployee())
                    Else
                        clsEmpleado.DownEmployee(dni)
                        DgvEmpl.Refresh()
                        MessageBox.Show("El empleado ha sido dado de baja debido a que tiene campos registrados", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ListAllEmployees(clsEmpleado.ListEmployee())
                    End If

                Catch ex As Exception
                    MessageBox.Show("No se pudo eliminar al empleado: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            End If

        End If


    End Sub
    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        Try
            Dim dni = TxtBuscar.Text
            ListAllEmployees(clsEmpleado.SearchEmployee(dni))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub SetDataForEditing()
        TxtDni.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(0).Value)
        TxtNombre.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(1).Value)
        TxtLastname.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(2).Value)
        CbxSex.SelectedIndex = IIf(Convert.ToString(DgvEmpl.CurrentRow.Cells(3).Value) = "M", 1, 0)
        TxtAdress.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(4).Value)
        TxtPhone.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(5).Value)
        TxtEmail.Text = Convert.ToString(DgvEmpl.CurrentRow.Cells(6).Value)
        RbtnState.Checked = Convert.ToString(DgvEmpl.CurrentRow.Cells(7).Value)
        TxtDni.Enabled = False
        BtnRegister.Text = "Modificar"
    End Sub

    Private Sub TxtBuscar_TextChanged(sender As Object, e As EventArgs) Handles TxtBuscar.TextChanged
        Try
            'If TxtBuscar.TextLength = 0 Or TxtBuscar.Text = Nothing Then
            '    ListAllEmployees(clsEmpleado.SearchEmployee(TxtBuscar.Text))
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class