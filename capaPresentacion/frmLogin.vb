﻿Imports capaNegocio
Public Class frmLogin
    Private users As capaDatos.Users
    Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click
        Try
            User = New clsUsuario().Login(txtUsuario.Text, txtPassword.Text)
            Dim username = User.Employee.Name + " " + User.Employee.Lastname
            MessageBox.Show("Bienvenido al Sistema " + username, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ClearData()
            txtUsuario.Focus()
        End Try
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub frmLogin_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        ClearData()
    End Sub
    Private Sub ClearData()
        txtUsuario.Clear()
        txtPassword.Clear()
    End Sub
    Public Property User() As capaDatos.Users
        Get
            Return users
        End Get
        Set(value As capaDatos.Users)
            users = value
        End Set
    End Property
End Class