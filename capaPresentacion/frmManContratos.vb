﻿Imports capaNegocio
Public Class frmManContratos

    Private contrat As capaDatos.Contract

    Private Sub frmManContratos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtIdBuscar.Enabled = False
        ListTable()
    End Sub

    Private Sub SetNewContrat()
        contrat = New capaDatos.Contract
        contrat.Dni = txtDni.Text
        contrat.Mount = Decimal.Parse(txtSalario.Text)
        contrat.StartDate = dtpInicio.Value
        contrat.FinishDate = dtpFin.Value
        contrat.ExtraHours = rbHorasExtra.Checked
        contrat.State = rbHorasExtra.Checked
    End Sub


    Private Sub BtnRegister_Click(sender As Object, e As EventArgs) Handles BtnRegister.Click
        If BtnRegister.Text = "Modificar" Then
            clsContrato.Update(Integer.Parse(txtIdBuscar.Text), dtpInicio.Value, dtpFin.Value, txtSalario.Text, rbHorasExtra.Checked, rbEstado.Checked)
            ClearControls()
            ListTable()
            MessageBox.Show("Modificación exitosa", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            BtnRegister.Text = "Registrar"
        Else
            If (txtDni.TextLength <> 8 Or txtSalario.TextLength = 0) Then
                MessageBox.Show("Ingrese correctamente los datos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                If (txtDni.TextLength = 8) Then
                    Try
                        Dim count As Integer
                        count = clsEmpleado.FindDni(txtDni.Text)
                        If count = 1 Then
                            SetNewContrat()
                            clsContrato.RegisterContrat(contrat)
                            ClearControls()
                            ListTable()
                            MessageBox.Show("Registro Exitoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            MessageBox.Show("El DNI ingresado no corresponde a ningún empleado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Catch ex As Exception
                        MessageBox.Show("Error al registrar: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                Else
                    MessageBox.Show("No exite un empleado registrado con ese DNI", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If

    End Sub

    Private Sub Delete()
        Try
            clsContrato.Delete(Integer.Parse(txtIdBuscar.Text))
            ClearControls()
            ListTable()
            MessageBox.Show("Contrato eliminado correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error al eliminar contrato: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub UpdateContrat()
        Dim c = clsContrato.FindContrat(Integer.Parse(txtIdBuscar.Text))
        dtpInicio.Value = c.startDate
        dtpFin.Value = c.finishDate
        txtSalario.Text = c.mount
        txtDni.Text = c.dni
        rbHorasExtra.Checked = c.extraHours
        rbEstado.Checked = c.state
        BtnRegister.Text = "Modificar"
    End Sub

    Private Sub Down()
        Try
            clsContrato.Down(Integer.Parse(txtIdBuscar.Text))
            ClearControls()
            ListTable()
            MessageBox.Show("Contrato desactivado correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Error al desactivar contrato: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub AddButtonColumn(text As String)
        Dim bt As New DataGridViewButtonColumn With {
            .Text = text,
            .Name = "Btn" & text,
            .UseColumnTextForButtonValue = True,
            .FlatStyle = FlatStyle.Flat
        }
        bt.CellTemplate.Style.ForeColor = Color.Black
        dgvContrat.Columns.Add(bt)

    End Sub

    Private Sub ListTable()
        Try
            dgvContrat.Columns.Clear()
            dgvContrat.DataSource = clsContrato.ListContrat()
            dgvContrat.Columns(0).HeaderText = "Id"
            dgvContrat.Columns(1).HeaderText = "Fecha Inicio"
            dgvContrat.Columns(2).HeaderText = "Fecha Salida"
            dgvContrat.Columns(3).HeaderText = "Salario"
            dgvContrat.Columns(4).HeaderText = "Estado"
            dgvContrat.Columns(5).HeaderText = "Horas Extras"
            dgvContrat.Columns(6).HeaderText = "DNI"
            dgvContrat.Columns.Remove("Employee")
            AddButtonColumn("Desactivar")
            AddButtonColumn("Modificar")
            'AddButtonColumn("Eliminar")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Sub


    Private Sub ListTableByDni(Dni As String)
        Try
            dgvContrat.Columns.Clear()
            dgvContrat.DataSource = clsContrato.FindContratByDni(Dni)
            dgvContrat.Columns(0).HeaderText = "Id"
            dgvContrat.Columns(1).HeaderText = "Fecha Inicio"
            dgvContrat.Columns(2).HeaderText = "Fecha Salida"
            dgvContrat.Columns(3).HeaderText = "Salario"
            dgvContrat.Columns(4).HeaderText = "Estado"
            dgvContrat.Columns(5).HeaderText = "Horas Extras"
            dgvContrat.Columns(6).HeaderText = "DNI"
            dgvContrat.Columns.Remove("Employee")
            AddButtonColumn("Desactivar")
            AddButtonColumn("Modificar")
            'AddButtonColumn("Eliminar")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click

        Try
            ListTableByDni(txtDniBuscar.Text)
            'txtDniBuscar.Text = ""
        Catch ex As Exception
            MessageBox.Show("Error al buscar contrato: " + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ClearControls()
        txtDni.Clear()
        txtSalario.Clear()
        dtpInicio.Value = DateTime.Now
        dtpFin.Value = DateTime.Now
    End Sub

    Private Sub dgvContrat_CellClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles dgvContrat.CellClick
        txtIdBuscar.Text = Convert.ToString(dgvContrat.CurrentRow.Cells(0).Value)
        Dim senderGrid = DirectCast(sender, DataGridView)
        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            If e.ColumnIndex = 7 Then
                Down()

            ElseIf e.ColumnIndex = 8 Then
                UpdateContrat()

                'ElseIf e.ColumnIndex = 9 Then
                '    Delete()

            End If
        End If
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class