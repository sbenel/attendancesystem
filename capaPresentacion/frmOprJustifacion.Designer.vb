﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOprJustifacion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOprJustifacion))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.BtnRegisterSchedule = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.RbtState = New System.Windows.Forms.RadioButton()
        Me.TxtReason = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.DgvAttendances = New System.Windows.Forms.DataGridView()
        Me.TxtDniForSearch = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.DgvAttendances, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.BtnRegisterSchedule)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.RbtState)
        Me.Panel1.Controls.Add(Me.TxtReason)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.DgvAttendances)
        Me.Panel1.Controls.Add(Me.TxtDniForSearch)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Location = New System.Drawing.Point(-2, -2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(842, 658)
        Me.Panel1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(619, 137)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(68, 28)
        Me.Button1.TabIndex = 77
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'BtnRegisterSchedule
        '
        Me.BtnRegisterSchedule.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.BtnRegisterSchedule.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnRegisterSchedule.FlatAppearance.BorderSize = 0
        Me.BtnRegisterSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnRegisterSchedule.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnRegisterSchedule.ForeColor = System.Drawing.Color.White
        Me.BtnRegisterSchedule.Location = New System.Drawing.Point(642, 584)
        Me.BtnRegisterSchedule.Name = "BtnRegisterSchedule"
        Me.BtnRegisterSchedule.Size = New System.Drawing.Size(144, 32)
        Me.BtnRegisterSchedule.TabIndex = 74
        Me.BtnRegisterSchedule.Text = "Registrar justificación"
        Me.BtnRegisterSchedule.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(111, 556)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 17)
        Me.Label3.TabIndex = 73
        Me.Label3.Text = "Estado:"
        '
        'RbtState
        '
        Me.RbtState.AutoSize = True
        Me.RbtState.Checked = True
        Me.RbtState.Location = New System.Drawing.Point(190, 554)
        Me.RbtState.Name = "RbtState"
        Me.RbtState.Size = New System.Drawing.Size(59, 19)
        Me.RbtState.TabIndex = 72
        Me.RbtState.TabStop = True
        Me.RbtState.Text = "Activo"
        Me.RbtState.UseVisualStyleBackColor = True
        '
        'TxtReason
        '
        Me.TxtReason.Location = New System.Drawing.Point(190, 396)
        Me.TxtReason.Multiline = True
        Me.TxtReason.Name = "TxtReason"
        Me.TxtReason.Size = New System.Drawing.Size(596, 122)
        Me.TxtReason.TabIndex = 71
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(53, 396)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 17)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Ingrese el motivo"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(14, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(441, 37)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Administración de justificaciones"
        '
        'DgvAttendances
        '
        Me.DgvAttendances.AllowUserToAddRows = False
        Me.DgvAttendances.AllowUserToDeleteRows = False
        Me.DgvAttendances.AllowUserToOrderColumns = True
        Me.DgvAttendances.AllowUserToResizeColumns = False
        Me.DgvAttendances.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvAttendances.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DgvAttendances.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        Me.DgvAttendances.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DgvAttendances.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DgvAttendances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvAttendances.DefaultCellStyle = DataGridViewCellStyle2
        Me.DgvAttendances.EnableHeadersVisualStyles = False
        Me.DgvAttendances.GridColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvAttendances.Location = New System.Drawing.Point(56, 246)
        Me.DgvAttendances.Name = "DgvAttendances"
        Me.DgvAttendances.ReadOnly = True
        Me.DgvAttendances.RowHeadersVisible = False
        Me.DgvAttendances.RowHeadersWidth = 51
        Me.DgvAttendances.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.DgvAttendances.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.White
        Me.DgvAttendances.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(42, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.DgvAttendances.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvAttendances.Size = New System.Drawing.Size(730, 134)
        Me.DgvAttendances.TabIndex = 68
        '
        'TxtDniForSearch
        '
        Me.TxtDniForSearch.Location = New System.Drawing.Point(297, 137)
        Me.TxtDniForSearch.Name = "TxtDniForSearch"
        Me.TxtDniForSearch.Size = New System.Drawing.Size(264, 23)
        Me.TxtDniForSearch.TabIndex = 67
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(53, 218)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(201, 17)
        Me.Label21.TabIndex = 64
        Me.Label21.Text = "Seleccione la tardanza a justificar"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(53, 137)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(238, 17)
        Me.Label17.TabIndex = 66
        Me.Label17.Text = "Listar asistencias por DNI de empleado"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(35, 105)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(132, 21)
        Me.Label18.TabIndex = 65
        Me.Label18.Text = "Datos generales"
        '
        'frmOprJustifacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 645)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(852, 692)
        Me.MinimizeBox = False
        Me.Name = "frmOprJustifacion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Justificaciones"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DgvAttendances, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents DgvAttendances As DataGridView
    Friend WithEvents TxtDniForSearch As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents TxtReason As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents RbtState As RadioButton
    Friend WithEvents BtnRegisterSchedule As Button
    Friend WithEvents Button1 As Button
End Class
