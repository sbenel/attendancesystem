USE MASTER
GO

IF exists (SELECT * FROM sysdatabases WHERE name='BDAsistencia')
	DROP DATABASE BDAsistencia
GO

CREATE DATABASE BDAsistencia
GO

USE BDAsistencia
GO

CREATE TABLE employee (
	dni 			CHAR(8) 			NOT NULL 	PRIMARY KEY,
	name 			VARCHAR(30) 		NOT NULL,
	lastname 		VARCHAR(60) 		NOT NULL,
	genre 			CHAR(1) 			NOT NULL,
	address 		VARCHAR(50) 		NOT NULL,
	phone 			VARCHAR(13) 		NOT NULL,
	email 			VARCHAR(50) 		NULL,
	state 			BIT 				NOT NULL
)
GO

CREATE TABLE contract (
	id 				INTEGER 			NOT NULL 	PRIMARY KEY IDENTITY,
	startDate 		DATE 				NOT NULL,
	finishDate 		DATE 				NOT NULL,
	mount 			DECIMAL(8, 2) 		NOT NULL,
	state 			BIT 				NOT NULL,
	extraHours 		BIT 				NOT NULL,
	dni 			CHAR(8) 			NOT NULL 	REFERENCES employee
)
GO

CREATE TABLE schedule (
	id 				INTEGER 			NOT NULL 	PRIMARY KEY IDENTITY,
	startDate 		DATE 				NOT NULL,
	finishDate 		DATE 				NULL,
	state 			BIT 				NOT NULL,
	dni 			CHAR(8) 			NOT NULL 	REFERENCES employee
)
GO

CREATE TABLE scheduleDetail (
	id 				INTEGER 			NOT NULL 	PRIMARY KEY IDENTITY,
	scheduleId 		INTEGER 			NOT NULL,
	day 			TINYINT 			NOT NULL,
	inHour 			TINYINT 			NOT NULL,
	outHour 		TINYINT 			NOT NULL
)
GO

ALTER TABLE scheduleDetail ADD CONSTRAINT FK_SD_SCH foreign key (scheduleId) REFERENCES schedule
GO

CREATE TABLE assistance (
	assistanceId 	INTEGER 			NOT NULL 	PRIMARY KEY IDENTITY,
	fecha 			DATE 				NOT NULL,
	inHour 			TIME 				NOT NULL,
	outHour 		TIME 				NULL,
	dni 			CHAR(8) 			NOT NULL
)
GO

ALTER TABLE assistance ADD CONSTRAINT FK_ASSIS_EMP FOREIGN KEY (dni) REFERENCES employee
GO

CREATE TABLE justification (
	justificationId 	INTEGER			NOT NULL 	PRIMARY KEY IDENTITY,
	fecha 				DATE 			NOT NULL,
	reason 				VARCHAR(100) 	NOT NULL,
	state 				BIT				NOT NULL,
	assistanceId 		INTEGER			NOT NULL
)
GO

ALTER TABLE justification ADD CONSTRAINT FK_JUS_ASSIS FOREIGN KEY (assistanceId) REFERENCES assistance
GO

CREATE TABLE permission (
	permissionId 		INTEGER	 		NOT NULL 	PRIMARY KEY IDENTITY,
	presentationDate 	DATE 			NOT NULL,
	permissionDate 		DATE 			NOT NULL,
	reason 				VARCHAR(100) 	NOT NULL,
	state 				BIT				NOT NULL,
	dni 				CHAR(8) 		NOT NULL
)
GO

ALTER TABLE permission ADD CONSTRAINT FK_PER_EMP FOREIGN KEY (dni) REFERENCES employee
GO

CREATE TABLE licenseType (
	licenseTypeId 		TINYINT 		NOT NULL 	PRIMARY KEY IDENTITY,
	description 		VARCHAR(50) 	NOT NULL,
	maxDias 			SMALLINT 		NOT NULL
) 
GO

CREATE TABLE license (
	lincenseId 			INTEGER 		NOT NULL 	PRIMARY KEY IDENTITY,
	startDate 			DATE 			NOT NULL,
	endDate 			DATE 			NOT NULL,
	state 				BIT 			NOT NULL,
	document 			VARCHAR(50)		NULL,
	dni 				CHAR(8) 		NOT NULL,
	typeId 				TINYINT 		NOT NULL
)
GO

ALTER TABLE License ADD CONSTRAINT FK_LIC_TYPE FOREIGN KEY (typeId) REFERENCES licenseType
GO

ALTER TABLE License ADD CONSTRAINT FK_LIC_EMP FOREIGN KEY (dni) REFERENCES employee
GO

CREATE TABLE users (
	userId 				INTEGER 		NOT NULL 	PRIMARY KEY IDENTITY,
	uSerName 			VARCHAR(30) 	NOT NULL,
	userPassword 		VARCHAR(30) 	NOT NULL,
	dni 				CHAR(8) 		NOT NULL,
	userState 			BIT 			NOT NULL
) 
GO

ALTER TABLE users ADD CONSTRAINT FK_US_EMP FOREIGN KEY (dni) REFERENCES employee
GO

--Insert
INSERT INTO employee 
	VALUES('12345678','Juan','Perez Castro',1,'Panamericana 123','974763945','juan_pc@gmail.com',1);
INSERT INTO employee 
	VALUES('69699696','LeTongue','Orgullo Peruano',1,'Tu corazon','987654321','letongue@gmail.com',1);
INSERT INTO Users 
	VALUES ('Juanpc','123456','12345678',1);
insert into Users values ('letongue','tonglish','69699696', 1)

INSERT INTO LicenseType 
	VALUES ('Licencia por maternidad pre y post natal',98);
INSERT INTO LicenseType 
	VALUES ('Licencia por paternidad',10);
INSERT INTO LicenseType 
	VALUES ('Licencia por enfermemdad de un familiar',7);
INSERT INTO LicenseType 
	VALUES ('Licencia por adopción',30);
INSERT INTO LicenseType 
	VALUES ('Licencia por lactancia materna',12);
INSERT INTO LicenseType 
VALUES ('Licencia por incapacidad temporal',340);

--agregando una columna que falto
ALTER TABLE License add PresentationDate date not null


--TRIGGERS 

--Contrato 
 CREATE TRIGGER StateChangeContractTrigger ON Contract
	AFTER INSERT AS
	DECLARE @id INT;
	DECLARE @cardid CHAR(8);
	DECLARE @iid INT;
BEGIN
	SELECT @iid = id, @cardid = dni  FROM inserted;
	SELECT @id = id FROM Contract WHERE State = 1 AND dni = @cardid AND id <> @iid;
	IF @id IS NOT NULL
		UPDATE Contract SET State = 0 WHERE id = @id;
END
GO


--Usuario
 
 CREATE TRIGGER StateChangeUserTrigger ON Users
	AFTER INSERT AS
	DECLARE @id INT;
	DECLARE @cardid CHAR(8);
	DECLARE @iid INT;
BEGIN
	SELECT @iid = userId , @cardid = dni  FROM inserted;
	SELECT @id = userId FROM Users WHERE userState = 1 AND dni = @cardid AND userId <> @iid;
	IF @id IS NOT NULL
		UPDATE Users SET userState = 0 WHERE id = @id;
END
GO



--Horario

 CREATE TRIGGER StateChangeScheduleTrigger ON Schedule
	AFTER INSERT AS
	DECLARE @id INT;
	DECLARE @cardid CHAR(8);
	DECLARE @iid INT;
BEGIN
	SELECT @iid = id, @cardid = dni  FROM inserted;
	SELECT TOP 1 @id = id FROM Schedule WHERE State = 1 AND dni = @cardid AND id <> @iid ORDER BY id;
	IF @id IS NOT NULL
		UPDATE Schedule SET State = 0 WHERE id = @id;
END
GO

--alter para poner inhour como not null
ALTER TABLE assistance
ALTER COLUMN inHour time null; 

--ASISTENCIAS
SELECT employee.lastname, employee.name, assistance.*
FROM assistance INNER JOIN employee ON assistance.dni = employee.dni
WHERE assistance.fecha < GETDATE() AND assistance.inHour IS NOT NULL

--FALTAS
SELECT assistance.dni,employee.lastname,employee.name,assistance.fecha,assistance.inHour
FROM assistance INNER JOIN employee ON assistance.dni = employee.dni
WHERE fecha < GETDATE() - 1 AND inHour Is Null 

--TARDANZAS
SELECT	schedule.dni, employee.name, employee.lastname, assistance.fecha, scheduleDetail.inHour, assistance.inHour, DATEDIFF(Minute,scheduleDetail.inHour,assistance.inHour) AS 'Minutos'
FROM	schedule INNER JOIN scheduleDetail ON schedule.id = scheduleDetail.scheduleId
		INNER JOIN assistance ON assistance.dni = schedule.dni
		INNER JOIN employee ON employee.dni = schedule.dni
WHERE	schedule.state = 1 AND  assistance.fecha < GETDATE() AND assistance.inHour IS NOT NULL

SELECT * FROM assistance
SELECT * FROM contract
SELECT * FROM employee
SELECT * FROM justification
SELECT * FROM license
SELECT * FROM licenseType
SELECT * FROM permission
SELECT * FROM schedule
SELECT * FROM scheduleDetail
SELECT * FROM users
